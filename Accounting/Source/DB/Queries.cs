﻿using System;

using System.Collections.Generic;

using System.Data.SqlClient;

namespace Accounting.Source.DB
{
    struct Commands
    {
        public static SqlCommand command;
        public static SqlDataReader reader;

        static Commands()
        {
            command = new SqlCommand();
            command.Connection = Program.database.DBConnection;
        }

        #region Read commands
        public static String rCategory = "SELECT * FROM [dbo].[Category]";
        public static String rSubcategory = "SELECT * FROM [dbo].[Subcategory]";
        public static String rGoods = "SELECT * FROM [dbo].[Goods]";
        public static String rGood = "SELECT Category FROM [dbo].[Goods] WHERE [ID]=@id";
        public static String rOrder = "SELECT * FROM [dbo].[Order]";
        public static String rOrderStatistic = "SELECT * FROM [dbo].[vOrderGoodsInfo] WHERE [ID]=@id";
        public static String rLogPricing = "SELECT * FROM [dbo].[PricingLogFile]";
        public static String rSerials = "SELECT * FROM [dbo].[vSerials]";
        #endregion

        #region Write commands
        public static String wCategory = "INSERT INTO [dbo].[Category] VALUES(@name)";
        public static String wSubcategory = "INSERT INTO [dbo].[Subcategory] VALUES(@id_category, @name)";
        public static String wGoods = "INSERT INTO [dbo].[Goods] VALUES(@id_category, @name, @count, @priceb, @prices, @warranty)";

        #region Order
        public static String wOrder = "INSERT INTO [dbo].[Order] VALUES(@date, @sum)";
        public static String wSerial = "INSERT INTO [dbo].[Serials] VALUES(@id_orderList, @id_good, @sn)";
        public static String wOrderList = "INSERT INTO [dbo].[OrderList] VALUES(@id_order, @id_good, @count)";
        #endregion

        #endregion

        #region Update commands
        public static String uCategory = "UPDATE [dbo].[Category] SET [Name]=@name WHERE [ID]=@id";
        public static String uSubcategory = "UPDATE [dbo].[Subcategory] SET [Name]=@name where [ID]=@id";
        public static String uGoods = "UPDATE [dbo].[Goods] SET [Category]=@id_category,"
            + " [Name]=@name, [Count]=@count, [PriceBuy]=@priceb, [PriceSell]=@prices,"
            + " [Warranty]=@warranty WHERE [ID]=@id";
        #endregion

        #region Delete commands
        public static String dCategory = "DELETE FROM [dbo].[Category] WHERE [ID]=@id";
        public static String dSubcategory = "DELETE FROM [dbo].[Subcategory] WHERE [ID]=@id";
        public static String dGoods = "DELETE FROM [dbo].[Goods] WHERE [ID]=@id";
        #endregion
    }

    public static class Read
    {
        public static LinkedList<Source.Types.Category> Category()
        {
            Program.database.OpenConnection();

            LinkedList<Source.Types.Category> data = new LinkedList<Source.Types.Category>();

            Commands.command.CommandText = Commands.rCategory;

            Commands.reader = Commands.command.ExecuteReader();

            while (Commands.reader.Read())
                data.AddLast(new Source.Types.Category(Commands.reader.GetInt32(0),
                                                       Commands.reader.GetString(1)));

            Program.database.CloseConnection();

            return data;
        }

        public static LinkedList<Source.Types.Subcategory> Subcategory()
        {
            Program.database.OpenConnection();

            LinkedList<Source.Types.Subcategory> data = new LinkedList<Source.Types.Subcategory>();

            Commands.command.CommandText = Commands.rSubcategory;
            Commands.reader = Commands.command.ExecuteReader();

            while (Commands.reader.Read())
                data.AddLast(new Source.Types.Subcategory(Commands.reader.GetInt32(0),
                                                          Commands.reader.GetInt32(1),
                                                          Commands.reader.GetString(2)));

            Program.database.CloseConnection();

            return data;
        }

        public static LinkedList<Source.Types.Good> Goods()
        {
            Program.database.OpenConnection();

            LinkedList<Source.Types.Good> data = new LinkedList<Source.Types.Good>();

            Commands.command.CommandText = Commands.rGoods;

            Commands.reader = Commands.command.ExecuteReader();

            while (Commands.reader.Read())
            {
                Source.Types.Good good =
                        new Source.Types.Good(Commands.reader.GetInt32(0),
                                              Commands.reader.GetInt32(1),
                                              Commands.reader.GetString(2),
                                              Commands.reader.GetInt32(3),
                                              Commands.reader.GetDecimal(4),
                                              Commands.reader.GetDecimal(5),
                                                    Commands.reader.IsDBNull(6)
                                                        ? (int?)null
                                                        : Commands.reader.GetInt32(6));

                data.AddLast(good);
            }

            Program.database.CloseConnection();

            return data;
        }

        public static Int32? Good(Int32 id)
        {
            Program.database.OpenConnection();

            Commands.command.CommandText = Commands.rGood;

            Commands.command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

            Commands.reader = Commands.command.ExecuteReader();

            if (Commands.reader.Read())
            {

                Int32 category = Commands.reader.GetInt32(0);

                Commands.command.Parameters.Clear();

                Program.database.CloseConnection();

                return category;
            }
            else
            {
                Commands.command.Parameters.Clear();

                Program.database.CloseConnection();
                return null;
            }
        }
        
        public static LinkedList<Source.Types.Order> Order()
        {
            Commands.command.CommandText = Commands.rOrder;

            LinkedList<Source.Types.Order> data = new LinkedList<Types.Order>();

            Program.database.OpenConnection();

            Commands.reader = Commands.command.ExecuteReader();

            while(Commands.reader.Read())
            {
                Source.Types.Order res = new Types.Order(Commands.reader.GetInt32(0),
                    Commands.reader.GetDateTime(1), Commands.reader.GetDecimal(2));

                data.AddLast(res);
            }

            Program.database.CloseConnection();

            return data;
        }

        public static LinkedList<Source.Types.OrderInfo> OrderInfo(Int32 id_order)
        {
            Commands.command.CommandText = Commands.rOrderStatistic;

            LinkedList<Source.Types.OrderInfo> data = new LinkedList<Source.Types.OrderInfo>();

            Program.database.OpenConnection();

            Commands.command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id_order;

            Commands.reader = Commands.command.ExecuteReader();

            while (Commands.reader.Read())
            {
                Source.Types.OrderInfo res = new Source.Types.OrderInfo(Commands.reader.GetString(0),
                    Commands.reader.GetString(1), Commands.reader.GetString(2), Commands.reader.GetInt32(3));

                data.AddLast(res);
            }

            Commands.command.Parameters.Clear();

            Program.database.CloseConnection();

            return data;
        }

        public static LinkedList<Source.Types.PricingLog> LogPricing()
        {
            Commands.command.CommandText = Commands.rLogPricing;

            LinkedList<Source.Types.PricingLog> data = new LinkedList<Source.Types.PricingLog>();

            Program.database.OpenConnection();

            Commands.reader = Commands.command.ExecuteReader();

            while (Commands.reader.Read())
            {
                Source.Types.PricingLog res = new Source.Types.PricingLog(Commands.reader.GetInt32(0), Commands.reader.GetDateTime(1),
                    Commands.reader.GetDecimal(2), Commands.reader.GetDecimal(3));

                data.AddLast(res);
            }

            Program.database.CloseConnection();

            return data;
        }

        public static LinkedList<Source.Types.SerialReading> Serials()
        {
            Commands.command.CommandText = Commands.rSerials;

            LinkedList<Source.Types.SerialReading> data = new LinkedList<Source.Types.SerialReading>();

            Program.database.OpenConnection();

            Commands.reader = Commands.command.ExecuteReader();

            while (Commands.reader.Read())
            {
                Source.Types.SerialReading res = null;
                try
                {
                    res = new Source.Types.SerialReading(Commands.reader.GetString(0), Commands.reader.GetString(1),
                        Commands.reader.GetDateTime(2), Commands.reader.GetInt32(3));
                }
                catch
                {
                    Message.Show.Information(Commands.reader.GetString(0));
                    Message.Show.Information(Commands.reader.GetString(1));
                    Message.Show.Information(Commands.reader.GetDateTime(2).ToShortDateString());
                    Message.Show.Information(Commands.reader.GetInt32(3).ToString());
                }

                data.AddLast(res);
            }

            Program.database.CloseConnection();

            return data;
        }
    }

    public static class Write
    {
        public static Source.Types.Category Category(String name)
        {
            Program.database.OpenConnection();

            Commands.command.CommandText = Commands.wCategory;

            Commands.command.Parameters.Add("@name", System.Data.SqlDbType.VarChar).Value = name;

            Commands.command.CommandType = System.Data.CommandType.Text;
            Commands.command.ExecuteNonQuery();

            Commands.command.CommandText = "SELECT TOP 1 [ID] FROM [dbo].[Category] ORDER BY [ID] DESC";

            Commands.reader = Commands.command.ExecuteReader();

            if (!Commands.reader.Read())
                return null;
            Int32 id = Commands.reader.GetInt32(0);

            Source.Types.Category added = new Types.Category(id, name);

            Commands.command.Parameters.Clear();

            Program.database.CloseConnection();

            return added;
        }

        public static Source.Types.Subcategory Subcategory(Int32 idCategory, String name)
        {
            Program.database.OpenConnection();

            Commands.command.CommandText = Commands.wSubcategory;

            Commands.command.Parameters.Add("@id_category", System.Data.SqlDbType.Int).Value = idCategory;
            Commands.command.Parameters.Add("@name", System.Data.SqlDbType.VarChar).Value = name;

            Commands.command.CommandType = System.Data.CommandType.Text;
            Commands.command.ExecuteNonQuery();

            Commands.command.CommandText = "SELECT TOP 1 [ID] FROM [dbo].[Subcategory] ORDER BY [ID] DESC";

            SqlDataReader read = Commands.command.ExecuteReader();

            if (!read.Read())
                return null;
            Int32 id = read.GetInt32(0);

            Source.Types.Subcategory added = new Types.Subcategory(id, idCategory, name);

            Commands.command.Parameters.Clear();

            Program.database.CloseConnection();

            return added;
        }
        
        public static Source.Types.Good Goods(Int32 idCategory, String name, Int32 count,
                                              Decimal priceB, Decimal priceS, Int32? warranty)
        {
            Program.database.OpenConnection();

            Commands.command.CommandText = Commands.wGoods;

            Commands.command.Parameters.Add("@id_category", System.Data.SqlDbType.Int).Value = idCategory;
            Commands.command.Parameters.Add("@name", System.Data.SqlDbType.VarChar).Value = name;
            Commands.command.Parameters.Add("@count", System.Data.SqlDbType.Int).Value = count;
            Commands.command.Parameters.Add("@priceb", System.Data.SqlDbType.Money).Value = priceB;
            Commands.command.Parameters.Add("@prices", System.Data.SqlDbType.Money).Value = priceS;

            if (!warranty.HasValue)
                Commands.command.Parameters.Add("@warranty", System.Data.SqlDbType.Int).Value = DBNull.Value;
            else
                Commands.command.Parameters.Add("@warranty", System.Data.SqlDbType.Int).Value = warranty.Value;

            Commands.command.CommandType = System.Data.CommandType.Text;
            Commands.command.ExecuteNonQuery();

            Commands.command.CommandText = "SELECT TOP 1 [ID] FROM [dbo].[Goods] ORDER BY [ID] DESC";

            SqlDataReader read = Commands.command.ExecuteReader();

            if (!read.Read())
                return null;
            Int32 id = read.GetInt32(0);

            Source.Types.Good added = new Types.Good(id, idCategory, name, count, priceB, priceS, warranty);

            Commands.command.Parameters.Clear();

            Program.database.CloseConnection();

            return added;
        }
        
        
        public static Source.Types.Order Order(DateTime date, Decimal sum)
        {
            Program.database.OpenConnection();

            Commands.command.CommandText = Commands.wOrder;

            Commands.command.Parameters.Add("@date", System.Data.SqlDbType.DateTime).Value = date;
            Commands.command.Parameters.Add("@sum", System.Data.SqlDbType.Money).Value = sum;

            Commands.command.CommandType = System.Data.CommandType.Text;
            Commands.command.ExecuteNonQuery();

            Commands.command.CommandText = "SELECT TOP 1 [ID] FROM [dbo].[Order] ORDER BY [ID] DESC";

            SqlDataReader read = Commands.command.ExecuteReader();

            if (!read.Read())
                return null;
            Int32 id = read.GetInt32(0);

            Source.Types.Order added = new Types.Order(id, date, sum);

            Commands.command.Parameters.Clear();

            Program.database.CloseConnection();

            return added;
        }

        public static Source.Types.OrderList OrderList(Int32 id_order, Int32 id_good, Int32 count)
        {
            Program.database.OpenConnection();

            Commands.command.CommandText = Commands.wOrderList;

            Commands.command.Parameters.Add("@id_order", System.Data.SqlDbType.Int).Value = id_order;
            Commands.command.Parameters.Add("@id_good", System.Data.SqlDbType.Int).Value = id_good;
            Commands.command.Parameters.Add("@count", System.Data.SqlDbType.Int).Value = count;

            Commands.command.CommandType = System.Data.CommandType.Text;
            Commands.command.ExecuteNonQuery();

            Commands.command.CommandText = "SELECT TOP 1 [ID] FROM [dbo].[OrderList] ORDER BY [ID] DESC";

            SqlDataReader read = Commands.command.ExecuteReader();

            if (!read.Read())
                return null;
            Int32 id = read.GetInt32(0);

            Source.Types.OrderList added = new Types.OrderList(id, id_order, id_good, count);

            Commands.command.Parameters.Clear();

            Program.database.CloseConnection();

            return added;
        }

        public static Source.Types.SerialAdding Serials(Int32 id_orderList, Int32 id_good, String sn)
        {
            Program.database.OpenConnection();

            Commands.command.CommandText = Commands.wSerial;

            Commands.command.Parameters.Add("@id_orderList", System.Data.SqlDbType.Int).Value = id_orderList;
            Commands.command.Parameters.Add("@id_good", System.Data.SqlDbType.Int).Value = id_good;
            Commands.command.Parameters.Add("@sn", System.Data.SqlDbType.VarChar).Value = sn;

            Commands.command.CommandType = System.Data.CommandType.Text;
            Commands.command.ExecuteNonQuery();

            Commands.command.CommandText = "SELECT TOP 1 [ID] FROM [dbo].[Order] ORDER BY [ID] DESC";

            SqlDataReader read = Commands.command.ExecuteReader();

            if (!read.Read())
                return null;
            Int32 id = read.GetInt32(0);

            Source.Types.SerialAdding added = new Types.SerialAdding(id, id_orderList, id_good, sn);

            Commands.command.Parameters.Clear();

            Program.database.CloseConnection();

            return added;
        }
    }

    public static class Update
    {
        public static void Category(String name, Int32 id)
        {
            Program.database.OpenConnection();

            Commands.command.CommandText = Commands.uCategory;

            Commands.command.Parameters.Add("@name", System.Data.SqlDbType.VarChar).Value = name;
            Commands.command.Parameters.Add("@ID", System.Data.SqlDbType.Int).Value = id;

            Commands.command.CommandType = System.Data.CommandType.Text;
            Commands.command.ExecuteNonQuery();

            Commands.command.Parameters.Clear();

            Program.database.CloseConnection();
        }

        public static void Subcategory(String name, Int32 id)
        {
            Program.database.OpenConnection();

            Commands.command.CommandText = Commands.uSubcategory;

            Commands.command.Parameters.Add("@name", System.Data.SqlDbType.VarChar).Value = name;
            Commands.command.Parameters.Add("@ID", System.Data.SqlDbType.Int).Value = id;

            Commands.command.CommandType = System.Data.CommandType.Text;
            Commands.command.ExecuteNonQuery();

            Commands.command.Parameters.Clear();

            Program.database.CloseConnection();
        }
        
        public static void Goods(Source.Types.Good item)
        {
            Program.database.OpenConnection();

            Commands.command.CommandText = Commands.uGoods;

            Commands.command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = item.Id;
            Commands.command.Parameters.Add("@id_category", System.Data.SqlDbType.Int).Value = item.IdCategory;
            Commands.command.Parameters.Add("@name", System.Data.SqlDbType.VarChar).Value = item.Name;
            Commands.command.Parameters.Add("@count", System.Data.SqlDbType.Int).Value = item.Count;
            Commands.command.Parameters.Add("@priceb", System.Data.SqlDbType.Money).Value = item.PriceBuy;
            Commands.command.Parameters.Add("@prices", System.Data.SqlDbType.Money).Value = item.PriceSell;

            if (!item.Warranty.HasValue)
                Commands.command.Parameters.Add("@warranty", System.Data.SqlDbType.Int).Value = DBNull.Value;
            else
                Commands.command.Parameters.Add("@warranty", System.Data.SqlDbType.Int).Value = item.Warranty;

            Commands.command.CommandType = System.Data.CommandType.Text;
            Commands.command.ExecuteNonQuery();

            Commands.command.Parameters.Clear();

            Program.database.CloseConnection();
        }
    }

    public static class Delete
    {
        public static void Category(Int32 id)
        {
            Program.database.OpenConnection();

            Commands.command.CommandText = Commands.dCategory;

            Commands.command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

            Commands.command.CommandType = System.Data.CommandType.Text;
            Commands.command.ExecuteNonQuery();

            Commands.command.Parameters.Clear();

            Program.database.CloseConnection();
        }

        public static void Subcategory(Int32 id)
        {
            Program.database.OpenConnection();

            Commands.command.CommandText = Commands.dSubcategory;

            Commands.command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

            Commands.command.CommandType = System.Data.CommandType.Text;
            Commands.command.ExecuteNonQuery();

            Commands.command.Parameters.Clear();

            Program.database.CloseConnection();
        }
        
        public static void Goods(Int32 id)
        {
            try
            {
                Program.database.OpenConnection();

                Commands.command.CommandText = Commands.dGoods;

                Commands.command.Parameters.Add("@id", System.Data.SqlDbType.Int).Value = id;

                Commands.command.CommandType = System.Data.CommandType.Text;
                Commands.command.ExecuteNonQuery();

                Commands.command.Parameters.Clear();
            }
            finally
            {
                Program.database.CloseConnection();
            }
        }
    }
}
