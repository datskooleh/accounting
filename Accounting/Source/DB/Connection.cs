﻿using System;
using System.Data.SqlClient;

using System.Configuration;

namespace Accounting.Source.DBConnection
{
    public class Connection : IDisposable
    {
        private SqlConnection database;
        public Boolean IsConnected { get; private set; }
        public Boolean IsOpened { get; private set; }

        public SqlConnection DBConnection { get { return database; } }

        private Boolean TryConnect()
        {
            String path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

            database = new SqlConnection("Data Source=(LocalDB)\\v11.0;AttachDbFilename="+@path+"\\accounting_db.mdf;Integrated Security=True");
            try
            {
                DBConnection.Open();
            }
            catch (SqlException ex)
            {
                DBConnection.Close();
                database = null;
                Message.Show.Error(String.Format("Не вдалося з'єднатися з базою.\n" + ex.Message));
                return false;
            }
            finally
            {
                if (DBConnection != null)
                    DBConnection.Close();
            }

            return true;
        }

        public void ConnectToDB()
        {
            IsConnected = TryConnect();
        }

        public void OpenConnection()
        {
            try
            {
                if (DBConnection != null)
                {
                    if (!IsOpened)
                    {
                        DBConnection.Open();
                        IsOpened = true;
                    }
                }
            }
            catch (SqlException ex)
            {
                Message.Show.Error("Не можливо відкрити з'єднання", "Помилка бази даних");
                IsOpened = false;
            }
        }

        public void CloseConnection()
        {
            try
            {
                if (DBConnection != null)
                {
                    if (IsOpened)
                    {
                        DBConnection.Close();
                        IsOpened = false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Message.Show.Error("Не можливо закрити з'єднання", "Помилка бази даних");
            }
        }

        public void Dispose()
        {
            if (IsConnected)
            {
                if (IsOpened)
                    database.Close();

                database.Dispose();
            }
        }
    }
}
