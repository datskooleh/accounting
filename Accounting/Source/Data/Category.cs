﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Accounting.Source.Data
{
    public class Category : IDisposable
    {
        private static Category owner;

        private readonly List<Source.Types.Category> _category;
        private readonly List<Source.Types.Subcategory> _subcategory;

        private Category()
        {
            _category = new List<Types.Category>();
            _subcategory = new List<Types.Subcategory>();

            LinkedList<Source.Types.Category> cRead = new LinkedList<Types.Category>();

            cRead = Source.DB.Read.Category();
            _category.AddRange(cRead);

            Program.database.CloseConnection();

            LinkedList<Source.Types.Subcategory> sRead = new LinkedList<Types.Subcategory>();

            sRead = Source.DB.Read.Subcategory();
            _subcategory.AddRange(sRead);

            Program.database.CloseConnection();
        }

        public static Category Init()
        {
            if (owner == null)
                owner = new Category();

            return owner;
        }

        public void Nullify()
        {
            this.Dispose();
        }

        public List<Source.Types.Category> Main { get { return _category; } }

        public List<Source.Types.Subcategory> Sub { get { return _subcategory; } }

        public void Dispose()
        {
            owner = null;
        }
    }
}
