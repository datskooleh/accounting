﻿using System;

namespace Accounting.Source.Types
{
    public sealed class Subcategory
    {   
        private Int32 _id;
        private Int32 _idCategory;
        private String _name;

        public Subcategory(Int32 id, Int32 idCategory, String name)
        {
            _id = id;
            _idCategory = idCategory;
            _name = name;
        }

        public Int32 Id { get { return _id; } }
        public Int32 IdCategory { get { return _idCategory; } }
        public String Name { get { return _name; } set { if (value.Length > 0) _name = value; } }
    
    }
}
