﻿using System;

namespace Accounting.Source.Types
{
    public sealed class Good
    {
        private Int32 _id;
        private Int32 _idCategory;
        private String _name;
        private Int32 _count;
        private Decimal _priceBuy;
        private Decimal _priceSell;
        private Int32? _warranty;

        public Int32 Id { get { return _id; } }

        public Int32 IdCategory { get { return _idCategory; } set { _idCategory = value; } }

        public String Name { get { return _name; } set { if (value.Length > 0) _name = value; } }

        public Int32 Count { get { return _count; } set { if (value >= 0) _count = value; } }

        public Decimal PriceBuy { get { return _priceBuy; } set { if (value >= 0) _priceBuy = value; } }

        public Decimal PriceSell { get { return _priceSell; } set { if (value >= 0) _priceSell = value; } }

        public Int32? Warranty { get { return _warranty; } set { _warranty = value; } }

        public Good(Int32 id, Int32 idCategory, String name, Int32 count,
                    Decimal priceBuy, Decimal priceSell, Int32? warranty)
        {
            _id = id;
            IdCategory = idCategory;
            Name = name;
            Count = count;
            PriceBuy = priceBuy;
            PriceSell = priceSell;

            if (warranty.HasValue)
                Warranty = warranty;
            else
                Warranty = null;
        }
    }
}
