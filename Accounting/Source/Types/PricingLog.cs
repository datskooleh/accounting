﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Source.Types
{
    public sealed class PricingLog
    {
        private Int32 _id;
        private String _name;
        private DateTime _date;
        private Decimal _oldPrice;
        private Decimal _newPrice;

        public PricingLog(Int32 id, DateTime date, Decimal oprice, Decimal nprice)
        {
            _id = id;
            _name = "";
            Date = date;
            OldPrice = oprice;
            NewPrice = nprice;
        }

        public void SetName()
        {
            foreach (Source.Types.Good good in Source.DB.Read.Goods())
                if (good.Id == _id)
                    _name = good.Name;
        }

        public String Name { get { return _name; } private set { _name = value; } }
        public DateTime Date { get { return _date; } private set { _date = value; } }
        public Decimal OldPrice { get { return _oldPrice; } private set { _oldPrice = value; } }
        public Decimal NewPrice { get { return _newPrice; } private set { _newPrice = value; } }
    }
}
