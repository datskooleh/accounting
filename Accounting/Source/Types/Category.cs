﻿using System;

namespace Accounting.Source.Types
{
    public sealed class Category
    {
        private Int32 _id;
        private String _name;

        public Category(Int32 id, String name)
        {
            _id = id;
            _name = name;
        }

        public Int32 Id { get { return _id; } }
        public String Name { get { return _name; } set { if (value.Length > 0) _name = value; } }
    }
}
