﻿using System;

namespace Accounting.Source.Types
{
    public sealed class SerialReading
    {
        private String _sn;
        private String _good;
        private DateTime _beginDate;
        private DateTime _endDate;

        public SerialReading(String sn, String good, DateTime beginDate, Int32 endDate)
        {
            SN = sn;
            Good = good;
            _beginDate = beginDate;
            _endDate = _beginDate.AddMonths(endDate);
        }

        public String SN { get { return _sn; } set { _sn = value; } }
        public String Good { get { return _good; } private set { _good = value; } }
        public String BeginDate { get { return _beginDate.ToShortDateString(); } }
        public String EndDate { get { return _endDate.ToShortDateString(); } }
    }
}
