﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Source.Types
{
    public sealed class OrderInfo
    {
        private String _categoryName;
        private String _subcategoryName;
        private String _goodName;
        private Int32 _count;

        public OrderInfo(String cn, String scn, String gn, Int32 c)
        {
            _categoryName = cn;
            _subcategoryName = scn;
            _goodName = gn;
            _count = c;
        }

        public String CategoryName { get { return _categoryName; } }

        public String SubcategoryName { get { return _subcategoryName; } }

        public String GoodName { get { return _goodName; } }

        public Int32 Count { get { return _count; } }
    }
}
