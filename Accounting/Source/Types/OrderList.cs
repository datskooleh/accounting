﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Source.Types
{
    public sealed class OrderList
    {
        private Int32 _id;
        private Int32 _idOrder;
        private Int32 _idGood;
        private Int32 _count;

        public OrderList(
            Int32 id,
            Int32 idOrder,
            Int32 idGood,
            Int32 count)
        {
            Id = id;
            IdOrder = idOrder;
            IdGood = idGood;
            Count = count;
        }

        public Int32 Id { get { return _id; } private set { _id = value; } }
        public Int32 IdOrder { get { return _idOrder; } private set { _idOrder = value; } }
        public Int32 IdGood { get { return _idGood; } private set { _idGood = value; } }
        public Int32 Count { get { return _count; } private set { _count = value; } }
    }
}
