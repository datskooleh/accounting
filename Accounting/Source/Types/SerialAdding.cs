﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Source.Types
{
    public sealed class SerialAdding
    {
        private Int32 _id;
        private Int32 _idOrderList;
        private Int32 _idGood;
        private String _sn;

        public SerialAdding(Int32 id, Int32 idOrderList, Int32 idGood, String sn)
        {
            _id = id;
            _sn = sn;
        }

        public Int32 Id { get { return _id; } private set { _id = value; } }

        public Int32 IdOrderList { get { return _idOrderList; } private set { _idOrderList = value; } }

        public Int32 IdGood { get { return _idGood; } private set { _idGood = value; } }

        public String SN { get { return _sn; } set { _sn = value; } }
    }
}
