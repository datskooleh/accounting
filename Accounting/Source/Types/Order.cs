﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accounting.Source.Types
{
    public sealed class Order
    {
        private Int32 _id;
        private DateTime _date;
        private Decimal _sum;

        public Order(Int32 id, DateTime date, Decimal sum)
        {
            _id = id;
            Date = date;
            Sum = sum;
        }

        public Int32 Id { get { return _id; } }

        public DateTime Date { get { return _date; } set { _date = value; } }

        public Decimal Sum { get { return _sum; } set { _sum = value; } }
    }
}
