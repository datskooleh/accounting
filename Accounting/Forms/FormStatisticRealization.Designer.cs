﻿namespace Accounting.Forms
{
    partial class FormStatisticRealization
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.orderResults = new System.Windows.Forms.DataGridView();
            this.columnDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnSum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderResultsGoods = new System.Windows.Forms.ListView();
            this.lvCategory = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvSubcategory = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvCount = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            ((System.ComponentModel.ISupportInitialize)(this.orderResults)).BeginInit();
            this.SuspendLayout();
            // 
            // orderResults
            // 
            this.orderResults.AllowUserToAddRows = false;
            this.orderResults.AllowUserToDeleteRows = false;
            this.orderResults.AllowUserToResizeRows = false;
            this.orderResults.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.orderResults.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.orderResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.orderResults.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnDate,
            this.columnSum});
            this.orderResults.Location = new System.Drawing.Point(2, 2);
            this.orderResults.MultiSelect = false;
            this.orderResults.Name = "orderResults";
            this.orderResults.ReadOnly = true;
            this.orderResults.RowHeadersVisible = false;
            this.orderResults.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.orderResults.Size = new System.Drawing.Size(225, 224);
            this.orderResults.TabIndex = 0;
            this.orderResults.SelectionChanged += new System.EventHandler(this.orderResults_SelectionChanged);
            // 
            // columnDate
            // 
            this.columnDate.HeaderText = "Дата";
            this.columnDate.Name = "columnDate";
            this.columnDate.ReadOnly = true;
            this.columnDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.columnDate.Width = 130;
            // 
            // columnSum
            // 
            this.columnSum.HeaderText = "Загалом";
            this.columnSum.Name = "columnSum";
            this.columnSum.ReadOnly = true;
            this.columnSum.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.columnSum.Width = 90;
            // 
            // orderResultsGoods
            // 
            this.orderResultsGoods.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.orderResultsGoods.BackColor = System.Drawing.SystemColors.Window;
            this.orderResultsGoods.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvCategory,
            this.lvSubcategory,
            this.lvName,
            this.lvCount});
            this.orderResultsGoods.FullRowSelect = true;
            this.orderResultsGoods.GridLines = true;
            this.orderResultsGoods.LabelWrap = false;
            this.orderResultsGoods.Location = new System.Drawing.Point(233, 2);
            this.orderResultsGoods.Name = "orderResultsGoods";
            this.orderResultsGoods.Size = new System.Drawing.Size(407, 224);
            this.orderResultsGoods.TabIndex = 1;
            this.orderResultsGoods.UseCompatibleStateImageBehavior = false;
            this.orderResultsGoods.View = System.Windows.Forms.View.Details;
            // 
            // lvCategory
            // 
            this.lvCategory.Text = "Категорія";
            this.lvCategory.Width = 120;
            // 
            // lvSubcategory
            // 
            this.lvSubcategory.Text = "Підкатегорія";
            this.lvSubcategory.Width = 120;
            // 
            // lvName
            // 
            this.lvName.Text = "Назва";
            this.lvName.Width = 100;
            // 
            // lvCount
            // 
            this.lvCount.Text = "Кількість";
            // 
            // FormStatisticRealization
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(644, 231);
            this.Controls.Add(this.orderResultsGoods);
            this.Controls.Add(this.orderResults);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormStatisticRealization";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Продажі";
            ((System.ComponentModel.ISupportInitialize)(this.orderResults)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView orderResults;
        private System.Windows.Forms.ListView orderResultsGoods;
        private System.Windows.Forms.ColumnHeader lvCategory;
        private System.Windows.Forms.ColumnHeader lvSubcategory;
        private System.Windows.Forms.ColumnHeader lvName;
        private System.Windows.Forms.ColumnHeader lvCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnSum;
    }
}