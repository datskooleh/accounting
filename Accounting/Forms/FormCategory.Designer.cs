﻿namespace Accounting.Forms
{
    partial class FormCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblArrow = new System.Windows.Forms.Label();
            this.tbCategory = new System.Windows.Forms.TextBox();
            this.btnAddCategory = new System.Windows.Forms.Button();
            this.btnAddSubcategory = new System.Windows.Forms.Button();
            this.tbSubcategory = new System.Windows.Forms.TextBox();
            this.categoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.categoryTable = new System.Windows.Forms.DataGridView();
            this.categoryNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoryID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subcategoryTable = new System.Windows.Forms.DataGridView();
            this.subcategoryNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subcategoryName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.in_categoryID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.subcategoryID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.accountingdbDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.subcategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subcategoryTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.accountingdbDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subcategoryBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // lblArrow
            // 
            this.lblArrow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblArrow.AutoSize = true;
            this.lblArrow.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArrow.Location = new System.Drawing.Point(254, 127);
            this.lblArrow.Name = "lblArrow";
            this.lblArrow.Size = new System.Drawing.Size(41, 39);
            this.lblArrow.TabIndex = 4;
            this.lblArrow.Text = "→";
            this.lblArrow.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbCategory
            // 
            this.tbCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCategory.Location = new System.Drawing.Point(3, 278);
            this.tbCategory.Name = "tbCategory";
            this.tbCategory.Size = new System.Drawing.Size(164, 23);
            this.tbCategory.TabIndex = 0;
            this.tbCategory.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbCategory_KeyUp);
            // 
            // btnAddCategory
            // 
            this.btnAddCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCategory.Location = new System.Drawing.Point(173, 278);
            this.btnAddCategory.Name = "btnAddCategory";
            this.btnAddCategory.Size = new System.Drawing.Size(75, 23);
            this.btnAddCategory.TabIndex = 1;
            this.btnAddCategory.Text = "Додати";
            this.btnAddCategory.UseVisualStyleBackColor = true;
            this.btnAddCategory.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btnAddCategory_KeyUp);
            this.btnAddCategory.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnAddCategory_MouseUp);
            // 
            // btnAddSubcategory
            // 
            this.btnAddSubcategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddSubcategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddSubcategory.Location = new System.Drawing.Point(298, 278);
            this.btnAddSubcategory.Name = "btnAddSubcategory";
            this.btnAddSubcategory.Size = new System.Drawing.Size(75, 23);
            this.btnAddSubcategory.TabIndex = 2;
            this.btnAddSubcategory.Text = "Додати";
            this.btnAddSubcategory.UseVisualStyleBackColor = true;
            this.btnAddSubcategory.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btnAddSubcategory_KeyUp);
            this.btnAddSubcategory.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnAddSubcategory_MouseUp);
            // 
            // tbSubcategory
            // 
            this.tbSubcategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSubcategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSubcategory.Location = new System.Drawing.Point(379, 278);
            this.tbSubcategory.Name = "tbSubcategory";
            this.tbSubcategory.Size = new System.Drawing.Size(164, 23);
            this.tbSubcategory.TabIndex = 3;
            this.tbSubcategory.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbSubcategory_KeyUp);
            // 
            // categoryTable
            // 
            this.categoryTable.AllowUserToAddRows = false;
            this.categoryTable.AllowUserToResizeColumns = false;
            this.categoryTable.AllowUserToResizeRows = false;
            this.categoryTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.categoryTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.categoryTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.categoryNumber,
            this.categoryName,
            this.categoryID});
            this.categoryTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.categoryTable.Location = new System.Drawing.Point(3, 3);
            this.categoryTable.MultiSelect = false;
            this.categoryTable.Name = "categoryTable";
            this.categoryTable.RowHeadersVisible = false;
            this.categoryTable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.categoryTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.categoryTable.Size = new System.Drawing.Size(245, 270);
            this.categoryTable.TabIndex = 7;
            this.categoryTable.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.categoryTable_CellEndEdit);
            this.categoryTable.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.categoryTable_CellMouseDoubleClick);
            this.categoryTable.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.categoryTable_CellValidating);
            this.categoryTable.SelectionChanged += new System.EventHandler(this.categoryTable_SelectionChanged);
            this.categoryTable.Sorted += new System.EventHandler(this.categoryTable_Sorted);
            this.categoryTable.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.categoryTable_UserDeletedRow);
            this.categoryTable.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.categoryTable_UserDeletingRow);
            // 
            // categoryNumber
            // 
            this.categoryNumber.HeaderText = "№";
            this.categoryNumber.Name = "categoryNumber";
            this.categoryNumber.ReadOnly = true;
            this.categoryNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.categoryNumber.Width = 40;
            // 
            // categoryName
            // 
            this.categoryName.HeaderText = "Категорії";
            this.categoryName.Name = "categoryName";
            this.categoryName.Width = 202;
            // 
            // categoryID
            // 
            this.categoryID.HeaderText = "Номер в базі";
            this.categoryID.Name = "categoryID";
            this.categoryID.Visible = false;
            // 
            // subcategoryTable
            // 
            this.subcategoryTable.AllowUserToAddRows = false;
            this.subcategoryTable.AllowUserToResizeColumns = false;
            this.subcategoryTable.AllowUserToResizeRows = false;
            this.subcategoryTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.subcategoryTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.subcategoryTable.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.subcategoryNumber,
            this.subcategoryName,
            this.in_categoryID,
            this.subcategoryID});
            this.subcategoryTable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.subcategoryTable.Location = new System.Drawing.Point(298, 3);
            this.subcategoryTable.MultiSelect = false;
            this.subcategoryTable.Name = "subcategoryTable";
            this.subcategoryTable.RowHeadersVisible = false;
            this.subcategoryTable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.subcategoryTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.subcategoryTable.Size = new System.Drawing.Size(245, 270);
            this.subcategoryTable.TabIndex = 8;
            this.subcategoryTable.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.subcategoryTable_CellEndEdit);
            this.subcategoryTable.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.subcategoryTable_CellMouseDoubleClick);
            this.subcategoryTable.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.subcategoryTable_CellValidating);
            this.subcategoryTable.Sorted += new System.EventHandler(this.subcategoryTable_Sorted);
            this.subcategoryTable.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.subcategoryTable_UserDeletedRow);
            this.subcategoryTable.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.subcategoryTable_UserDeletingRow);
            // 
            // subcategoryNumber
            // 
            this.subcategoryNumber.HeaderText = "№";
            this.subcategoryNumber.Name = "subcategoryNumber";
            this.subcategoryNumber.ReadOnly = true;
            this.subcategoryNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.subcategoryNumber.Width = 40;
            // 
            // subcategoryName
            // 
            this.subcategoryName.HeaderText = "Підкатегорії";
            this.subcategoryName.Name = "subcategoryName";
            this.subcategoryName.Width = 202;
            // 
            // in_categoryID
            // 
            this.in_categoryID.HeaderText = "Категорія";
            this.in_categoryID.Name = "in_categoryID";
            this.in_categoryID.Visible = false;
            // 
            // subcategoryID
            // 
            this.subcategoryID.HeaderText = "Ідентифікатор в базі";
            this.subcategoryID.Name = "subcategoryID";
            this.subcategoryID.Visible = false;
            // 
            // FormCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 306);
            this.Controls.Add(this.subcategoryTable);
            this.Controls.Add(this.categoryTable);
            this.Controls.Add(this.tbSubcategory);
            this.Controls.Add(this.btnAddSubcategory);
            this.Controls.Add(this.btnAddCategory);
            this.Controls.Add(this.tbCategory);
            this.Controls.Add(this.lblArrow);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormCategory";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Керування категоріями";
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subcategoryTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.accountingdbDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subcategoryBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblArrow;
        private System.Windows.Forms.TextBox tbCategory;
        private System.Windows.Forms.Button btnAddCategory;
        private System.Windows.Forms.Button btnAddSubcategory;
        private System.Windows.Forms.TextBox tbSubcategory;
        private System.Windows.Forms.BindingSource categoryBindingSource;
        private System.Windows.Forms.DataGridView categoryTable;
        private System.Windows.Forms.DataGridView subcategoryTable;
        private System.Windows.Forms.BindingSource accountingdbDataSetBindingSource;
        private System.Windows.Forms.BindingSource subcategoryBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn subcategoryNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn subcategoryName;
        private System.Windows.Forms.DataGridViewTextBoxColumn in_categoryID;
        private System.Windows.Forms.DataGridViewTextBoxColumn subcategoryID;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoryNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoryName;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoryID;
    }
}