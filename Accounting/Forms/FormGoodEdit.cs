﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Forms
{
    public partial class FormGoodEdit : Form
    {
        Source.Types.Good editingGood;
        Source.Data.Category category;

        public FormGoodEdit(Int32 id, Int32 idCategory, String name,
            Int32 count, Decimal priceb, Decimal prices, Int32? warranty)
        {
            InitializeComponent();

            editingGood = new Source.Types.Good(id, idCategory, name, count, priceb, prices, warranty);

            Init();
        }

        public FormGoodEdit(Source.Types.Good good)
        {
            InitializeComponent();

            category = Source.Data.Category.Init();

            editingGood = good;

            Init();
        }

        private void ReparseDropDownList(ComboBox main, ComboBox sub)
        {
            ddlSubcategory.Items.Clear();

            Int32 id = category.Main
                .Where(x => x.Name
                    .Equals(main.SelectedItem.ToString()))
                .Single()
                .Id;

            foreach (Source.Types.Subcategory item in category.Sub)
                if (id == item.IdCategory)
                    sub.Items.Add(item.Name);

            if (ddlSubcategory.Items.Count == 0)
            {
                Message.Show.Error("В даній категорії не існує підкатегорій");
                return;
            }
            else
                ddlSubcategory.SelectedIndex = 0;
        }

        private void btnConfirm_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                btnConfirm_KeyUp(sender, new KeyEventArgs(Keys.Enter));
        }

        private void btnCancel_MouseUp(object sender, MouseEventArgs e)
        {

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                btnCancel_KeyUp(sender, new KeyEventArgs(Keys.Enter));
        }

        private void btnConfirm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!IsValid())
                    return;

                Int32 mainId = category.Main[ddlCategory.SelectedIndex].Id;
                Int32 id_category = category.Sub
                    .Where(x => x.IdCategory == mainId)
                    .Skip(ddlSubcategory.SelectedIndex)
                    .Take(1)
                    .Single().Id;
                String name = tbName.Text.ToString();
                Int32 count = Convert.ToInt32(tbCount.Text.ToString());
                Decimal priceb = Convert.ToDecimal(tbPriceBuy.Text.ToString());
                Decimal prices = Convert.ToDecimal(tbPriceSell.Text.ToString());
                Int32? warranty = tbWarranty.Text.Length > 0
                    ? Convert.ToInt32(tbWarranty.Text.ToString())
                    : (Int32?)null;

                if (!ddlCategory.SelectedItem.ToString().Equals(lblCurrentCategory.Text.ToString())
                    || !ddlSubcategory.SelectedItem.ToString().Equals(lblCurrentSubcategory.Text.ToString())
                    || !name.Equals(editingGood.Name)
                    || count != editingGood.Count
                    || priceb != editingGood.PriceBuy
                    || prices != editingGood.PriceSell
                    || (warranty.HasValue && !editingGood.Warranty.HasValue
                        || !warranty.HasValue && editingGood.Warranty.HasValue
                        || warranty.Value != editingGood.Warranty.Value))
                {
                    Source.DB.Update.Goods(new Source.Types.Good(editingGood.Id, id_category,
                            name, count, priceb, prices, warranty));

                    editingGood.IdCategory = id_category;
                    editingGood.Name = name;
                    editingGood.Count = count;
                    editingGood.PriceBuy = priceb;
                    editingGood.PriceSell = prices;
                    editingGood.Warranty = warranty;

                    DialogResult = System.Windows.Forms.DialogResult.OK;
                }
                else
                    DialogResult = System.Windows.Forms.DialogResult.Abort;

                this.Close();
            }
        }

        private void btnCancel_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.Abort;
            }
        }

        private Boolean IsValid()
        {
            if (ddlSubcategory.SelectedIndex == -1)
            {
                Message.Show.Error("В даній категорії не існує підкатегорій");
                return false;
            }
            if (tbName.Text.Length == 0)
            {
                Message.Show.Error("Введіть назву товару");
                return false;
            }
            if (tbCount.Text.Length == 0)
            {
                Message.Show.Error("Повинна бути вказана кількість на складі, інакше поставне 0");
                return false;
            }
            if (tbPriceBuy.Text.Length == 0)
            {
                Message.Show.Error("Потрібно ввести ціну закупки чи 0 в іншому випадку");
                return false;
            }
            if (tbPriceSell.Text.Length == 0)
            {
                Message.Show.Error("Потрібно ввести ціну продажу чи 0 в іншому випадку");
                return false;
            }

            return true;
        }

        private void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReparseDropDownList(ddlCategory, ddlSubcategory);
        }

        private void tbCount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void tbPriceBuy_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsControl(e.KeyChar)
        && !Char.IsDigit(e.KeyChar)
        && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void tbPriceSell_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsControl(e.KeyChar)
        && !Char.IsDigit(e.KeyChar)
        && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void tbWarranty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;

            if (e.KeyChar == (Char)Keys.Enter)
                btnConfirm_KeyUp(sender, new KeyEventArgs((Keys)e.KeyChar));
        }

        private void Init()
        {
            foreach (Source.Types.Category cat in category.Main)
                ddlCategory.Items.Add(cat.Name);

            if (ddlCategory.Items.Count > 0)
            {
                Int32 mainID = category.Sub
                    .Where(x => x.Id == editingGood.IdCategory)
                    .Select(x => x.IdCategory)
                    .Single();

                String mainName = category.Main
                    .Where(x => x.Id == mainID)
                    .Select(x => x.Name)
                    .Single();

                String subName = category.Sub
                    .Where(x => x.Id == editingGood.IdCategory)
                    .Select(x => x.Name)
                    .Single();

                for (Int32 i = 0; i < ddlCategory.Items.Count; i++)
                    if (ddlCategory.Items[i].ToString().Equals(mainName))
                    {
                        ddlCategory.SelectedIndex = i;
                        break;
                    }

                if (ddlSubcategory.Items.Count > 0)
                {
                    for (Int32 i = 0; i < ddlSubcategory.Items.Count; i++)
                        if (ddlSubcategory.Items[i].ToString().Equals(subName))
                        {
                            ddlSubcategory.SelectedIndex = i;
                            break;
                        }

                    lblCurrentCategory.Text = ddlCategory.SelectedItem.ToString();
                    lblCurrentSubcategory.Text = ddlSubcategory.SelectedItem.ToString();
                }
            }

            tbIdInDatabase.Text = editingGood.Id.ToString();
            tbName.Text = editingGood.Name.ToString();
            tbCount.Text = editingGood.Count.ToString();
            tbPriceBuy.Text = editingGood.PriceBuy.ToString();
            tbPriceSell.Text = editingGood.PriceSell.ToString();

            if (editingGood.Warranty.HasValue)
                tbWarranty.Text = editingGood.Warranty.Value.ToString();
        }
    }
}
