﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace Accounting.Forms
{
    public partial class FormPriceChanging : Form
    {
        LinkedList<Source.Types.PricingLog> pricing;

        public FormPriceChanging()
        {
            InitializeComponent();

            pricing = Source.DB.Read.LogPricing();

            foreach (Source.Types.PricingLog item in pricing)
            {
                item.SetName();
                AddToTable(item.Name, item.Date, item.OldPrice, item.NewPrice);
            }
        }

        private void AddToTable(String name, DateTime date, Decimal oldp, Decimal newp)
        {
            Int32 rowIndex;
            DataGridViewRow row;

            rowIndex = tableGoods.Rows.Add();
            row = tableGoods.Rows[rowIndex];

            row.Cells[0].Value = name;
            row.Cells[1].Value = date;
            row.Cells[2].Value = oldp;
            row.Cells[3].Value = newp;
        }
    }
}
