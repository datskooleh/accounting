﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Forms
{
    public partial class FormGoodBuy : Form
    {
        Source.Data.Category category;

        public FormGoodBuy()
        {
            InitializeComponent();

            category = Source.Data.Category.Init();

            ddlCategory.Items.Add("Всі");

            foreach (Source.Types.Category item in category.Main)
            {
                ddlCategory.Items.Add(item.Name);
                ddlAddCategory.Items.Add(item.Name);
            }

            foreach (Source.Types.Subcategory item in category.Sub)
                ddlSubcategory.Items.Add(item.Name);

            ddlCategory.SelectedIndex = 0;
            ddlSubcategory.Enabled = false;
        }

        private void AddToGoodsTable(Source.Types.Good item)
        {
            int rowIndex;
            DataGridViewRow row;

            rowIndex = tableGoods.Rows.Add();
            row = tableGoods.Rows[rowIndex];
            row.Cells[0].Value = rowIndex + 1;
            row.Cells[1].Value = category.Sub.Where(x => x.Id == item.IdCategory).Single().Name;
            row.Cells[2].Value = item.Name;
            row.Cells[3].Value = item.Count;
            row.Cells[4].Value = item.PriceBuy;
            row.Cells[5].Value = item.PriceSell;
            row.Cells[6].Value = item.Warranty;
            row.Cells[7].Value = item.Id;
        }

        #region Add goods field parse
        private void tbAddWarranty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;

            if (e.KeyChar == (Char)Keys.Enter)
                btnAddConfirm_KeyPress(sender, e);
        }

        private void tbAddCount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void tbAddSellPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsControl(e.KeyChar)
        && !Char.IsDigit(e.KeyChar)
        && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void tbAddBuyPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
        && !Char.IsDigit(e.KeyChar)
        && e.KeyChar != '.')
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if (e.KeyChar == '.'
                && (sender as TextBox).Text.IndexOf('.') > -1)
            {
                e.Handled = true;
            }
        }

        private void btnAddConfirm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((Char)Keys.Enter == e.KeyChar)
            {
                if (!ValidateInput())
                    return;

                Int32 id = category.Sub
                    .Where(x => x.IdCategory == category.Main[ddlAddCategory.SelectedIndex].Id
                    && x.Name.Equals(ddlAddSubcategory.SelectedItem.ToString()))
                    .Select(x => x.Id)
                    .Single();

                Source.DB.Write.Goods(
                    id,
                    tbAddName.Text,
                    Convert.ToInt32(tbAddCount.Text),
                    Convert.ToDecimal(tbAddBuyPrice.Text),
                    Convert.ToDecimal(tbAddSellPrice.Text),
                    (tbAddWarranty.Text.Length == 0) ? (int?)null : Convert.ToInt32(tbAddWarranty.Text)
                    );
            }
        }

        private void btnAddConfirm_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                btnAddConfirm_KeyPress(sender, new KeyPressEventArgs((Char)Keys.Enter));
        }
        #endregion

        private Boolean ValidateInput()
        {
            if (ddlAddCategory.SelectedIndex == -1)
            {
                Message.Show.Error("Не вказана категорія");
                ddlAddCategory.Focus();
                return false;
            }
            else if (ddlAddSubcategory.SelectedIndex == -1)
            {
                Message.Show.Error("Не вказана підкатегорія");
                ddlSubcategory.Focus();
                return false;
            }
            else if (tbAddName.Text.Length == 0)
            {
                Message.Show.Error("Потрібно вказати назву");
                tbAddName.Focus();
                return false;
            }

            else if (tbAddCount.Text.Length == 0)
            {
                Message.Show.Error("Потрібно вказати кількість");
                tbAddCount.Focus();
                return false;
            }
            else if (tbAddBuyPrice.Text.Length == 0)
            {
                Message.Show.Error("Потрібно вказати ціну покупки");
                tbAddBuyPrice.Focus();
                return false;
            }
            else if (tbAddSellPrice.Text.Length == 0)
            {
                Message.Show.Error("Потрібно вказати ціну продажу");
                tbAddSellPrice.Focus();
                return false;
            }

            return true;
        }

        private void UpdateRowData(Source.Types.Good good)
        {
            tableGoods.SelectedRows[0].Cells[1].Value = category.Sub.Where(x => x.Id == good.IdCategory).Select(x => x.Name).Single();
            tableGoods.SelectedRows[0].Cells[2].Value = good.Name;
            tableGoods.SelectedRows[0].Cells[3].Value = good.Count;
            tableGoods.SelectedRows[0].Cells[4].Value = good.PriceBuy;
            tableGoods.SelectedRows[0].Cells[5].Value = good.PriceSell;
            if (good.Warranty.HasValue)
                tableGoods.SelectedRows[0].Cells[6].Value = good.Warranty;
            else
                tableGoods.SelectedRows[0].Cells[6].Value = null;
        }


        private void ddlAddCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlAddSubcategory.Items.Clear();

            if (category.Sub.Where(x => x.IdCategory == category.Main[ddlAddCategory.SelectedIndex].Id).Count() > 0)
                ReparseDropDownList(ddlAddCategory, ddlAddSubcategory);
        }

        private void ddlSubcategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            tableGoods.Rows.Clear();

            if (ddlCategory.SelectedIndex == 0)
                foreach (Source.Types.Good item in Source.DB.Read.Goods())
                    AddToGoodsTable(item);
            else if (ddlSubcategory.SelectedItem.ToString().Equals("Всі"))
            {
                Int32[] subIds = category.Sub
                    .Where(x => x.IdCategory == category.Main[ddlCategory.SelectedIndex - 1].Id)
                    .Select(x => x.Id)
                    .Distinct()
                    .ToArray();

                foreach (Source.Types.Good item in Source.DB.Read.Goods())
                    foreach (Int32 id in subIds)
                        if (item.IdCategory == id)
                            AddToGoodsTable(item);
            }
            else
            {
                Int32 subID = category.Sub
                    .Where(x=>x.IdCategory == category.Main[ddlCategory.SelectedIndex - 1].Id 
                    && x.Name.Equals(ddlSubcategory.SelectedItem.ToString()))
                    .Select(x => x.Id)
                    .Single();

                foreach (Source.Types.Good item in Source.DB.Read.Goods())
                    if (item.IdCategory == subID)
                        AddToGoodsTable(item);
            }
        }


        private void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlSubcategory.Items.Clear();

            ddlSubcategory.Items.Add("Всі");
            ddlSubcategory.SelectedIndex = 0;

            ReparseDropDownList(ddlCategory, ddlSubcategory);
        }


        private void ReparseDropDownList(ComboBox main, ComboBox sub)
        {
            if (main.SelectedItem.ToString().Equals("Всі"))
            {
                sub.Enabled = false;
                return;
            }

            sub.Enabled = true;

            if (sub.Items.Count > 0)
                if (sub.Items[0].ToString().Equals("Всі"))
                {
                    foreach (Source.Types.Subcategory item in category.Sub)
                        if (item.IdCategory == category.Main[ddlCategory.SelectedIndex - 1].Id)
                            sub.Items.Add(item.Name);
                    return;
                }

            Int32 id = category.Main
                .Where(x => x.Name
                    .Equals(main.SelectedItem.ToString()))
                .Single()
                .Id;

            foreach (Source.Types.Subcategory item in category.Sub)
                if (id == item.IdCategory)
                    sub.Items.Add(item.Name);
        }

        private void tableGoods_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Int32 id = Convert.ToInt32(tableGoods.Rows[e.RowIndex].Cells[7].Value.ToString());

            Int32? idCategory = Source.DB.Read.Good(id);

            if (!idCategory.HasValue)
            {
                Message.Show.Error("Не вдалося отримати товар");
                return;
            }

            Int32? warranty;

            try
            {
                warranty = Convert.ToInt32(tableGoods.Rows[e.RowIndex].Cells[6].Value.ToString());
            }
            catch(NullReferenceException ex)
            {
                warranty = null;
            }

            Source.Types.Good good = new Source.Types.Good(id, idCategory.Value,
                tableGoods.Rows[e.RowIndex].Cells[2].Value.ToString(),
                Convert.ToInt32(tableGoods.Rows[e.RowIndex].Cells[3].Value.ToString()),
                Convert.ToDecimal(tableGoods.Rows[e.RowIndex].Cells[4].Value.ToString()),
                Convert.ToDecimal(tableGoods.Rows[e.RowIndex].Cells[5].Value.ToString()),
                warranty);

            FormGoodEdit edit = new FormGoodEdit(good);
            edit.ShowDialog();

            if (edit.DialogResult == System.Windows.Forms.DialogResult.OK)
                UpdateRowData(good);
        }

        private void tableGoods_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            DialogResult decision =
                    Message.Show.Question(String.Format("Дійсно видалити {0}?", tableGoods.SelectedRows[0].Cells[2].Value.ToString()), "Видалення",
                    MessageBoxButtons.YesNo);

            if (decision == System.Windows.Forms.DialogResult.No)
            {
                e.Cancel = true;
                return;
            }

            try
            {
                Source.DB.Delete.Goods(Convert.ToInt32(tableGoods.SelectedRows[0].Cells[7].Value.ToString()));
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                Message.Show.Error("Не вдалося видалити товар");
                e.Cancel = true;
            }
        }
    }
}
