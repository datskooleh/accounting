﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Accounting
{
    public partial class FormMain : Form, IDisposable
    {
        public FormMain()
        {
            InitializeComponent();
            IsMdiContainer = true;
        }

        private void TSMExitExit_MouseUp(object sender, MouseEventArgs e)
        {
            Application.Exit();
        }

        private void TSMExitChange_MouseUp(object sender, MouseEventArgs e)
        {
            this.Close();
        }

        private void TSMBuys_MouseUp(object sender, MouseEventArgs e)
        {
            if (this.MdiChildren.Length > 2)
            {
                MessageBox.Show("Тільки 3 вкладені вікна дозволені одночасно","Попередження");
                return;
            }
            Forms.FormGoodBuy FormGoodBuyMdi = new Forms.FormGoodBuy();
            FormGoodBuyMdi.MdiParent = this;
            FormGoodBuyMdi.Show();
        }

        private void TSMExitInfo_MouseUp(object sender, MouseEventArgs e)
        {
            MessageBox.Show("Here must be window information about user. Inside you can edit it\'s info");
        }

        private void TSMRealizationSells_MouseUp(object sender, MouseEventArgs e)
        {
            if (this.MdiChildren.Length > 2)
            {
                MessageBox.Show("Тільки 3 вкладені вікна дозволені одночасно", "Попередження");
                return;
            }

            Forms.FormGoodSell FormGoodSellMdi = new Forms.FormGoodSell();
            FormGoodSellMdi.MdiParent = this;
            FormGoodSellMdi.Show();
        }

        private void TSMRealizationRepair_MouseUp(object sender, MouseEventArgs e)
        {
            Message.Show.Error("Must be implemented", "Nothing here");
        }

        private void TSMRealizationAssembly_MouseUp(object sender, MouseEventArgs e)
        {
            MessageBox.Show("Must be implemented");
        }

        private void TSMCategory_MouseUp(object sender, MouseEventArgs e)
        {
            if (this.MdiChildren.Length > 2)
            {
                MessageBox.Show("Тільки 3 вкладені вікна дозволені одночасно", "Попередження");
                return;
            }

            Forms.FormCategory FormCategoryMdi = new Forms.FormCategory();

            if (FormCategoryMdi.IsMdiChild)
            {
                MessageBox.Show("Is already active");
                return;
            }

            FormCategoryMdi.MdiParent = this;

            FormCategoryMdi.Show();
        }

        private void TSMReportRealization_MouseUp(object sender, MouseEventArgs e)
        {
            if (this.MdiChildren.Length > 2)
            {
                MessageBox.Show("Тільки 3 вкладені вікна дозволені одночасно", "Попередження");
                return;
            }

            Forms.FormStatisticRealization FormRealizationStatisticMdi = new Forms.FormStatisticRealization();
            FormRealizationStatisticMdi.MdiParent = this;
            FormRealizationStatisticMdi.Show();
        }

        private void історіяЗміниЦінToolStripMenuItem_MouseUp(object sender, MouseEventArgs e)
        {
            if (this.MdiChildren.Length > 2)
            {
                MessageBox.Show("Тільки 3 вкладені вікна дозволені одночасно", "Попередження");
                return;
            }

            Forms.FormPriceChanging FormPriceChangingMdi = new Forms.FormPriceChanging();
            FormPriceChangingMdi.MdiParent = this;
            FormPriceChangingMdi.Show();
        }

        private void серійніНомериToolStripMenuItem_MouseUp(object sender, MouseEventArgs e)
        {
            if (this.MdiChildren.Length > 2)
            {
                MessageBox.Show("Тільки 3 вкладені вікна дозволені одночасно", "Попередження");
                return;
            }

            Forms.FormSerialsStatistics FormSerialsMdi = new Forms.FormSerialsStatistics();
            FormSerialsMdi.MdiParent = this;
            FormSerialsMdi.Show();
        }
    }
}
