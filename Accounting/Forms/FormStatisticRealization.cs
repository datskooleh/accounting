﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Forms
{
    public partial class FormStatisticRealization : Form
    {
        LinkedList<Source.Types.Order> results = new LinkedList<Source.Types.Order>();

        public FormStatisticRealization()
        {
            InitializeComponent();

            results = Source.DB.Read.Order();

            foreach (Source.Types.Order order in results)
                AddToOrderTable(order);

            if (orderResults.Rows.Count > 0)
                orderResults.Rows[0].Selected = true;
        }

        private void AddToOrderTable(Source.Types.Order item)
        {
            int rowIndex;
            DataGridViewRow row;

            rowIndex = orderResults.Rows.Add();
            row = orderResults.Rows[rowIndex];
            row.Cells[0].Value = item.Date;
            row.Cells[1].Value = item.Sum;
        }

        private void orderResults_SelectionChanged(object sender, EventArgs e)
        {
            orderResultsGoods.Items.Clear();

            LinkedList<Source.Types.OrderInfo> orderInfo;

            orderInfo = Source.DB.Read.OrderInfo(results.ElementAt(orderResults.SelectedRows[0].Index).Id);
            
            foreach (Source.Types.OrderInfo item in orderInfo)
            {
                orderResultsGoods.Items.Add(item.CategoryName);

                orderResultsGoods.Items[orderResultsGoods.Items.Count - 1].SubItems.Add(item.SubcategoryName);
                orderResultsGoods.Items[orderResultsGoods.Items.Count - 1].SubItems.Add(item.GoodName);
                orderResultsGoods.Items[orderResultsGoods.Items.Count - 1].SubItems.Add(item.Count.ToString());
            }
        }
    }
}
