﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Forms
{
    public partial class FormSerialsStatistics : Form
    {
        public FormSerialsStatistics()
        {
            InitializeComponent();

            foreach (Source.Types.SerialReading item in Source.DB.Read.Serials())
                AddToTable(item);
        }

        private void AddToTable(Source.Types.SerialReading item)
        {
            int rowIndex;
            DataGridViewRow row;

            rowIndex = tableSerials.Rows.Add();
            row = tableSerials.Rows[rowIndex];
            row.Cells[0].Value = item.Good;
            row.Cells[1].Value = item.BeginDate;
            row.Cells[2].Value = item.EndDate;
            row.Cells[3].Value = item.SN;
        }
    }
}
