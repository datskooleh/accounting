﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Forms
{
    public partial class FormCategory : Form
    {
        private Source.Data.Category category;
        private Int32 previousCategoryIndex = -1;

        public FormCategory()
        {
            InitializeComponent();

            category = Source.Data.Category.Init();

            int rowIndex;
            DataGridViewRow row;

            rowIndex = categoryTable.Rows.Add();
            row = categoryTable.Rows[rowIndex];
            row.Cells[0].Value = "1";
            row.Cells[1].Value = "Всі";
            row.Cells[2].Value = "-1";

            foreach (Source.Types.Category item in category.Main)
            {
                AddToCategoryTable(item);
            }

            foreach (Source.Types.Subcategory item in category.Sub)
            {
                AddToSubcategoryTable(item);
            }
        }

        #region Add items to main category

        private void btnAddCategory_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!HasText(tbCategory.Text))
                {
                    Message.Show.Warning("Не задано назви підкатегорії категорії", "Попередження");
                    return;
                }
                
                if (IsInList(categoryTable, tbCategory.Text))
                {
                    Message.Show.Information("Дана категорія вже присутня", "Увага");
                    return;
                }

                int rowIndex;
                DataGridViewRow row;

                rowIndex = categoryTable.Rows.Add();
                row = categoryTable.Rows[rowIndex];

                String number = Convert.ToString(categoryTable.Rows.Count);

                category.Main.Add(Source.DB.Write.Category(tbCategory.Text));

                row.Cells[0].Value = number;
                row.Cells[1].Value = category.Main[category.Main.Count - 1].Name;
                row.Cells[2].Value = category.Main[category.Main.Count - 1].Id;

                tbCategory.Clear();
            }
        }

        private void btnAddCategory_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                btnAddCategory_KeyUp(sender, new KeyEventArgs(Keys.Enter));
        }

        #endregion

        #region Add items to subcategory

        private void btnAddSubcategory_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!HasText(tbSubcategory.Text))
                {
                    Message.Show.Warning("Не задано назви категорії", "Попередження");
                    return;
                }
                
                if (IsInList(subcategoryTable, tbSubcategory.Text))
                {
                    Message.Show.Information("Дана категорія вже присутня", "Увага");
                    return;
                }
                if (categoryTable.SelectedRows[0].Index == 0)
                {
                    Message.Show.Information("Не можливо додати в кореневу категорію \"Всі\". "
                        + "Виберіть іншу.", "Увага");
                    return;
                }

                int rowIndex;
                DataGridViewRow row;

                rowIndex = subcategoryTable.Rows.Add();
                row = subcategoryTable.Rows[rowIndex];

                String number = Convert.ToString(subcategoryTable.Rows.Count);

                category.Sub.Add(
                    Source.DB.Write.Subcategory(
                        Convert.ToInt32(categoryTable.SelectedRows[0].Cells[2].Value), tbSubcategory.Text));

                row.Cells[0].Value = number;
                row.Cells[1].Value = category.Sub[category.Sub.Count - 1].Name;
                row.Cells[2].Value = category.Sub[category.Sub.Count - 1].Id;
                row.Cells[3].Value = category.Sub[category.Sub.Count - 1].IdCategory;

                tbSubcategory.Clear();
            }
        }

        private void btnAddSubcategory_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                btnAddSubcategory_KeyUp(sender, new KeyEventArgs(Keys.Enter));
        }

        #endregion

        #region TextBox input control

        private void tbCategory_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnAddCategory_KeyUp(sender, e);
        }

        private void tbSubcategory_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnAddSubcategory_KeyUp(sender, e);
        }

        #endregion

        #region Before element deleted from table
        private void categoryTable_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (e.Row.Index == 0)
            {
                Message.Show.Error("Не можливо видалити загальну категорію");
                e.Cancel = true;
                return;
            }

            Boolean IsAlone = true;

            foreach (Source.Types.Subcategory item in category.Sub)
                if (item.IdCategory == Convert.ToInt32(e.Row.Cells[2].Value.ToString()))
                {
                    IsAlone = false;
                    break;
                }

            if (!IsAlone)
            {
                Message.Show.Error("Не можливо видалити категорію"
                    + " в яку входять підкатегорії. Видаліть спершу пов\'язані підкатегорії");
                e.Cancel = true;
                return;
            }

            Int32 id = Convert.ToInt32(e.Row.Cells[2].Value);

            Source.DB.Delete.Category(id);

            category.Main.Remove(category.Main.Select(x => x).Where(x => x.Id == id).Single());
        }

        private void subcategoryTable_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            foreach (Source.Types.Good item in Source.DB.Read.Goods())
                if (item.IdCategory == Convert.ToInt32(e.Row.Cells[2].Value.ToString()))
                {
                    Message.Show.Error("Не можливо видалити підкатегорію до якої відноситься товар");
                    e.Cancel = true;
                    return;
                }

            Source.DB.Delete.Subcategory(Convert.ToInt32(e.Row.Cells[2].Value));

            category.Sub.Remove(category.Sub
                            .Select(x => x)
                            .Where(x => x.Id == Convert.ToInt32(e.Row.Cells[2].Value))
                            .Single());

        }
        #endregion

        #region After deletion item
        private void subcategoryTable_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            ResetNumeration(subcategoryTable);
        }

        private void categoryTable_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            ResetNumeration(categoryTable);
        }
        #endregion

        #region Double row click handler 
        private void categoryTable_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                if (e.RowIndex != 0)
                    categoryTable.BeginEdit(false);
            }
        }

        private void subcategoryTable_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex == 1)
            {
                if (categoryTable.SelectedRows[0].Index == 0)
                {
                    Message.Show.Information("Редагування в загальній категорії заборонене");
                }
                else
                    subcategoryTable.BeginEdit(false);
            }
        }
        #endregion

        private void categoryTable_SelectionChanged(object sender, EventArgs e)
        {
            if (categoryTable.SelectedRows.Count == 0)
                return;

            if (previousCategoryIndex == categoryTable.SelectedRows[0].Index)
                return;

            previousCategoryIndex = categoryTable.SelectedRows[0].Index;

            ResetSubcategoryList();
        }

        #region Tables isertion data methods
        private void AddToCategoryTable(Source.Types.Category item)
        {
            int rowIndex;
            DataGridViewRow row;

            rowIndex = categoryTable.Rows.Add();
            row = categoryTable.Rows[rowIndex];
            row.Cells[0].Value = rowIndex + 1;
            row.Cells[1].Value = item.Name;
            row.Cells[2].Value = item.Id;
        }

        private void AddToSubcategoryTable(Source.Types.Subcategory item)
        {
            int rowIndex;
            DataGridViewRow row;

            rowIndex = subcategoryTable.Rows.Add();
            row = subcategoryTable.Rows[rowIndex];
            row.Cells[0].Value = rowIndex + 1;
            row.Cells[1].Value = item.Name;
            row.Cells[2].Value = item.Id;
            row.Cells[3].Value = item.IdCategory;
        }
        #endregion

        #region Edited cell validation and edit beginning
        private void categoryTable_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            DataGridView senderView = sender as DataGridView;

            if (senderView == null)
            {
                e.Cancel = true;
                Message.Show.Error("Не вдалося редагувати");
                return;
            }

            foreach (DataGridViewRow row in senderView.Rows)
            {
                if (row.Index != e.RowIndex && !row.IsNewRow)
                {
                    if (!HasText(e.FormattedValue.ToString()))
                    {
                        Message.Show.Error("Назва не може бути порожньою");
                        e.Cancel = true;
                        return;
                    }
                    else if (row.Cells[1].Value.ToString().ToLower() == e.FormattedValue.ToString().ToLower())
                    {
                        senderView.Rows[e.RowIndex].ErrorText =
                            "Назва повинна бути унікальною";
                        Message.Show.Error(senderView.Rows[e.RowIndex].ErrorText);
                        e.Cancel = true;
                        return;
                    }
                }
            }
            senderView.Rows[e.RowIndex].ErrorText = string.Empty;
        }

        private void subcategoryTable_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            categoryTable_CellValidating(sender, e);
        }
        #endregion

        #region After cell edited
        private void categoryTable_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Int32 id = Convert.ToInt32(categoryTable.Rows[e.RowIndex].Cells[2].Value.ToString());

            Source.DB.Update.Category(categoryTable.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(),
                id);

            category.Main.Select(x => x).Where(x => x.Id == id).Single().Name =
                categoryTable.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
        }

        private void subcategoryTable_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            Int32 id = Convert.ToInt32(subcategoryTable.Rows[e.RowIndex].Cells[2].Value.ToString());

            Source.DB.Update.Subcategory(subcategoryTable.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(),
                id);

            category.Sub.Select(x => x).Where(x => x.Id == id).Single().Name =
                subcategoryTable.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
        }
        #endregion

        #region After sort logic
        private void categoryTable_Sorted(object sender, EventArgs e)
        {
            ResetNumeration(sender as DataGridView);
        }

        private void subcategoryTable_Sorted(object sender, EventArgs e)
        {
            ResetNumeration(sender as DataGridView);
        }
        #endregion




        #region Support operations
        private Boolean HasText(String input)
        {
            return !String.IsNullOrWhiteSpace(input);
        }

        private Boolean IsInList(DataGridView view, String value)
        {
            for (int i = 0; i < view.Rows.Count; i++)
            {
                if (String.Equals(view.Rows[i].Cells[1].Value.ToString().ToLower(), value.ToLower()))
                    return true;
            }

            return false;
        }

        private void ResetNumeration(DataGridView view)
        {
            //or i = view.SelectedRows[0].Index
            for (int i = 0; i < view.RowCount; i++)
                view.Rows[i].Cells[0].Value = i + 1;
            view.Update();
        }

        private void ResetSubcategoryList()
        {
            subcategoryTable.Rows.Clear();

            if (categoryTable.SelectedRows[0].Index == 0)
                foreach (Source.Types.Subcategory item in category.Sub)
                    AddToSubcategoryTable(item);
            else
                foreach (Source.Types.Subcategory item in category.Sub)
                    if (item.IdCategory == Convert.ToInt32(categoryTable.SelectedRows[0].Cells[2].Value))
                        AddToSubcategoryTable(item);
        }
        #endregion
    }
}
