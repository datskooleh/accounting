﻿namespace Accounting.Forms
{
    partial class FormGoodSell
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSelectGoods = new System.Windows.Forms.Button();
            this.lblPay = new System.Windows.Forms.Label();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.tbPay = new System.Windows.Forms.TextBox();
            this.tableGoods = new System.Windows.Forms.DataGridView();
            this.tableCategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableSubcategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableWarranty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableSerials = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tablePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tablePriceTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableGoodId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableCategoryID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.tableGoods)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSelectGoods
            // 
            this.btnSelectGoods.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSelectGoods.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSelectGoods.Location = new System.Drawing.Point(2, 267);
            this.btnSelectGoods.Name = "btnSelectGoods";
            this.btnSelectGoods.Size = new System.Drawing.Size(175, 49);
            this.btnSelectGoods.TabIndex = 0;
            this.btnSelectGoods.Text = "Вибрати товар";
            this.btnSelectGoods.UseVisualStyleBackColor = true;
            this.btnSelectGoods.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btnSelectGoods_KeyUp);
            this.btnSelectGoods.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnSelectGoods_MouseUp);
            // 
            // lblPay
            // 
            this.lblPay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPay.AutoSize = true;
            this.lblPay.Location = new System.Drawing.Point(566, 279);
            this.lblPay.Name = "lblPay";
            this.lblPay.Size = new System.Drawing.Size(60, 13);
            this.lblPay.TabIndex = 10;
            this.lblPay.Text = "До оплати";
            // 
            // btnConfirm
            // 
            this.btnConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConfirm.Location = new System.Drawing.Point(647, 268);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(125, 48);
            this.btnConfirm.TabIndex = 4;
            this.btnConfirm.TabStop = false;
            this.btnConfirm.Text = "Оформити";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btnConfirm_KeyUp);
            this.btnConfirm.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnConfirm_MouseUp);
            // 
            // tbPay
            // 
            this.tbPay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbPay.Enabled = false;
            this.tbPay.Location = new System.Drawing.Point(541, 296);
            this.tbPay.MaxLength = 20;
            this.tbPay.Name = "tbPay";
            this.tbPay.Size = new System.Drawing.Size(100, 20);
            this.tbPay.TabIndex = 7;
            this.tbPay.TabStop = false;
            this.tbPay.Text = "0.0";
            this.tbPay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tableGoods
            // 
            this.tableGoods.AllowUserToAddRows = false;
            this.tableGoods.AllowUserToResizeRows = false;
            this.tableGoods.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableGoods.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableGoods.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tableCategory,
            this.tableSubcategory,
            this.tableName,
            this.tableCount,
            this.tableWarranty,
            this.tableSerials,
            this.tablePrice,
            this.tablePriceTotal,
            this.tableGoodId,
            this.tableCategoryID});
            this.tableGoods.Location = new System.Drawing.Point(2, 2);
            this.tableGoods.MultiSelect = false;
            this.tableGoods.Name = "tableGoods";
            this.tableGoods.RowHeadersVisible = false;
            this.tableGoods.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tableGoods.Size = new System.Drawing.Size(770, 260);
            this.tableGoods.TabIndex = 11;
            this.tableGoods.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.tableGoods_CellClick);
            this.tableGoods.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.tableGoods_UserDeletingRow);
            // 
            // tableCategory
            // 
            this.tableCategory.HeaderText = "Категорія";
            this.tableCategory.Name = "tableCategory";
            this.tableCategory.ReadOnly = true;
            this.tableCategory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tableSubcategory
            // 
            this.tableSubcategory.HeaderText = "Підкатегорія";
            this.tableSubcategory.Name = "tableSubcategory";
            this.tableSubcategory.ReadOnly = true;
            this.tableSubcategory.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tableName
            // 
            this.tableName.HeaderText = "Назва";
            this.tableName.Name = "tableName";
            this.tableName.ReadOnly = true;
            this.tableName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.tableName.Width = 150;
            // 
            // tableCount
            // 
            this.tableCount.HeaderText = "Кількість";
            this.tableCount.Name = "tableCount";
            this.tableCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tableWarranty
            // 
            this.tableWarranty.HeaderText = "Гарантія (міс.)";
            this.tableWarranty.Name = "tableWarranty";
            this.tableWarranty.ReadOnly = true;
            this.tableWarranty.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tableSerials
            // 
            this.tableSerials.HeaderText = "Серійні номери";
            this.tableSerials.Name = "tableSerials";
            this.tableSerials.ReadOnly = true;
            // 
            // tablePrice
            // 
            this.tablePrice.HeaderText = "Ціна од.";
            this.tablePrice.Name = "tablePrice";
            this.tablePrice.ReadOnly = true;
            this.tablePrice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tablePriceTotal
            // 
            this.tablePriceTotal.HeaderText = "Загалом";
            this.tablePriceTotal.Name = "tablePriceTotal";
            this.tablePriceTotal.ReadOnly = true;
            this.tablePriceTotal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tableGoodId
            // 
            this.tableGoodId.HeaderText = "ID товару";
            this.tableGoodId.Name = "tableGoodId";
            this.tableGoodId.ReadOnly = true;
            this.tableGoodId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.tableGoodId.Visible = false;
            // 
            // tableCategoryID
            // 
            this.tableCategoryID.HeaderText = "ID категорії";
            this.tableCategoryID.Name = "tableCategoryID";
            this.tableCategoryID.ReadOnly = true;
            this.tableCategoryID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.tableCategoryID.Visible = false;
            // 
            // FormGoodSell
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 322);
            this.Controls.Add(this.tableGoods);
            this.Controls.Add(this.tbPay);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.lblPay);
            this.Controls.Add(this.btnSelectGoods);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormGoodSell";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Реалізація";
            ((System.ComponentModel.ISupportInitialize)(this.tableGoods)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSelectGoods;
        private System.Windows.Forms.Label lblPay;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.TextBox tbPay;
        private System.Windows.Forms.DataGridView tableGoods;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableCategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableSubcategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableName;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableWarranty;
        private System.Windows.Forms.DataGridViewButtonColumn tableSerials;
        private System.Windows.Forms.DataGridViewTextBoxColumn tablePrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn tablePriceTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableGoodId;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableCategoryID;
    }
}