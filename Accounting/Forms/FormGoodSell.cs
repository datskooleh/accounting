﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Accounting.Forms
{
    public partial class FormGoodSell : Form
    {
        private List<Source.Types.Good> _allGoods;
        private LinkedList<LinkedList<String>> serials;
        private Source.Data.Category category;
        private LinkedList<Source.Types.Good> selectedGoods;

        public FormGoodSell()
        {
            InitializeComponent();

            _allGoods = new List<Source.Types.Good>();

            serials = new LinkedList<LinkedList<string>>();

            category = Source.Data.Category.Init();

            foreach (Source.Types.Good good in Source.DB.Read.Goods())
                _allGoods.Add(good);

            selectedGoods = new LinkedList<Source.Types.Good>();
        }

        private void btnSelectGoods_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LinkedList<String> localSerials = new LinkedList<string>();

                FormGoodSelect formListGoods = new FormGoodSelect(_allGoods, selectedGoods, localSerials);
                formListGoods.ShowDialog();

                if (formListGoods.DialogResult != System.Windows.Forms.DialogResult.OK)
                    return;

                Int32 categoryId = category.Main
                    .Where(x => x.Id == category.Sub
                        .Where(y => y.Id == selectedGoods.Last.Value.IdCategory)
                        .Select(y => y.IdCategory)
                        .Single())
                        .Select(x => x.Id)
                        .Single();

                int rowIndex;
                DataGridViewRow row;

                rowIndex = tableGoods.Rows.Add();
                row = tableGoods.Rows[rowIndex];
                row.Cells[0].Value = category.Main.Where(x => x.Id == categoryId).Select(x => x.Name).Single();
                row.Cells[1].Value = category.Sub.Where(x => x.Id == selectedGoods.Last.Value.IdCategory).Select(x => x.Name).Single();
                row.Cells[2].Value = selectedGoods.Last.Value.Name;
                row.Cells[3].Value = selectedGoods.Last.Value.Count;
                row.Cells[4].Value = selectedGoods.Last.Value.Warranty.HasValue ? selectedGoods.Last.Value.Warranty.Value : (Int32?)null;

                serials.AddLast(localSerials);

                if(localSerials.Count > 0)
                {
                    row.Cells[5].Value = "+";
                    row.Cells[5].Style.BackColor = Color.DarkGreen;
                    row.Cells[5].Style.ForeColor = Color.White;
                }
                else
                {
                    row.Cells[5].Value = "-";
                    row.Cells[5].Style.BackColor = Color.DarkRed;
                    row.Cells[5].Style.ForeColor = Color.White;
                }

                row.Cells[6].Value = selectedGoods.Last.Value.PriceSell;
                row.Cells[7].Value = selectedGoods.Last.Value.PriceSell * selectedGoods.Last.Value.Count;
                row.Cells[8].Value = selectedGoods.Last.Value.Id;
                row.Cells[9].Value = selectedGoods.Last.Value.IdCategory;

                tbPay.Text = Convert
                    .ToString(Math.Round(Convert
                        .ToDecimal(tbPay.Text) + selectedGoods.Last.Value.PriceSell * selectedGoods.Last.Value.Count, 2));
            }
        }

        private void btnConfirm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if(tableGoods.Rows.Count == 0)
                {
                    Message.Show.Information("Для оформлення покупки потрібно вибрати хоча б один товар");
                    return;
                }

                Source.Types.Order order;

                try
                {
                    order = Source.DB.Write.Order(DateTime.Now, Convert.ToDecimal(tbPay.Text));
                }
                catch (System.Data.SqlClient.SqlException ex)
                {
                    Message.Show.Error("Не можливо записати покупку в базу даних", "Помилка бази даних");
                    return;
                }

                Source.Types.OrderList orderList;

                Int32 index = 0;
                foreach (Source.Types.Good good in selectedGoods)
                {
                    orderList = Source.DB.Write.OrderList(order.Id, good.Id, good.Count);

                    if (good.Warranty.HasValue)
                    {
                        foreach (String serial in serials.ElementAt(index))
                                Source.DB.Write.Serials(orderList.Id, orderList.IdGood, serial);
                        ++index;
                    }

                    Source.DB.Update.Goods(_allGoods.Where(x => x.Id == good.Id).Single());
                }

                Message.Show.Information(String.Format("Транзакцію завершено. До оплати: {0}", tbPay.Text));
                tbPay.Text = "0.0";

                tableGoods.Rows.Clear();
            }
        }

        private void btnSelectGoods_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                btnSelectGoods_KeyUp(sender, new KeyEventArgs(Keys.Enter));
        }

        private void btnConfirm_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                btnConfirm_KeyUp(sender, new KeyEventArgs(Keys.Enter));
        }

        private void tbCount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void tableGoods_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            tbPay.Text = Convert
                .ToString(Convert
                    .ToDecimal(tbPay.Text) - Convert
                    .ToDecimal(tableGoods.SelectedRows[0].Cells[7].Value));
            Source.Types.Good good = _allGoods.Where(x => x.Id == Convert.ToInt32(e.Row.Cells[8].Value)).Single();
            good.Count += Convert.ToInt32(e.Row.Cells[3].Value);
            serials.Remove(serials.ElementAt(e.Row.Index));
        }

        private void tableGoods_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 5)
                return;

            LinkedList<String> listSerials = serials.ElementAt(e.RowIndex);

            try
            {
                if (listSerials.Count == 0)
                    throw new NullReferenceException();

                FormGoodSelectedSN formSerials = new FormGoodSelectedSN(listSerials,
                    Convert.ToInt32(tableGoods.Rows[e.RowIndex].Cells[3].Value), FormGoodSelectedSN.OpenType.Edit);
                formSerials.ShowDialog();
            }
            catch (NullReferenceException ex)
            {
                Int32? warranty = 0;
                try
                {
                    warranty = Convert.ToInt32(tableGoods.Rows[e.RowIndex].Cells[5].Value.ToString());
                }
                catch(FormatException _ex)
                {
                    warranty = null;
                }

                if (!warranty.HasValue)
                {
                    Message.Show.Information("Даний товар не має гарантійного терміну");
                    return;
                }

                DialogResult addWarranty = Message.Show.Question("Для даного товару не встановлено серійні номери. Зробити це зараз?",
                    "Додати серійні номери", MessageBoxButtons.YesNo);

                if (addWarranty == System.Windows.Forms.DialogResult.Yes)
                {
                    FormGoodSelectedSN formSerials = new FormGoodSelectedSN(listSerials,
                        Convert.ToInt32(tableGoods.Rows[e.RowIndex].Cells[3].Value), FormGoodSelectedSN.OpenType.Add);
                }
            }
        }
    }
}
