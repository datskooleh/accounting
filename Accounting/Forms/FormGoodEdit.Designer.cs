﻿namespace Accounting.Forms
{
    partial class FormGoodEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblID = new System.Windows.Forms.Label();
            this.lblCategory = new System.Windows.Forms.Label();
            this.lblSubcategory = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.lblPriceBuy = new System.Windows.Forms.Label();
            this.lblPriceSell = new System.Windows.Forms.Label();
            this.lblWarranty = new System.Windows.Forms.Label();
            this.tbIdInDatabase = new System.Windows.Forms.TextBox();
            this.ddlCategory = new System.Windows.Forms.ComboBox();
            this.ddlSubcategory = new System.Windows.Forms.ComboBox();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbCount = new System.Windows.Forms.TextBox();
            this.tbPriceBuy = new System.Windows.Forms.TextBox();
            this.tbPriceSell = new System.Windows.Forms.TextBox();
            this.tbWarranty = new System.Windows.Forms.TextBox();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblCurrentCategory = new System.Windows.Forms.Label();
            this.lblCurrentSubcategory = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Location = new System.Drawing.Point(4, 5);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(142, 13);
            this.lblID.TabIndex = 0;
            this.lblID.Text = "Ідентифікатор в базі даних";
            // 
            // lblCategory
            // 
            this.lblCategory.AutoSize = true;
            this.lblCategory.Location = new System.Drawing.Point(4, 52);
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Size = new System.Drawing.Size(56, 13);
            this.lblCategory.TabIndex = 1;
            this.lblCategory.Text = "Категорія";
            // 
            // lblSubcategory
            // 
            this.lblSubcategory.AutoSize = true;
            this.lblSubcategory.Location = new System.Drawing.Point(4, 101);
            this.lblSubcategory.Name = "lblSubcategory";
            this.lblSubcategory.Size = new System.Drawing.Size(71, 13);
            this.lblSubcategory.TabIndex = 2;
            this.lblSubcategory.Text = "Підкатегорія";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(4, 129);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(39, 13);
            this.lblName.TabIndex = 3;
            this.lblName.Text = "Назва";
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(4, 157);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(53, 13);
            this.lblCount.TabIndex = 4;
            this.lblCount.Text = "Кількість";
            // 
            // lblPriceBuy
            // 
            this.lblPriceBuy.AutoSize = true;
            this.lblPriceBuy.Location = new System.Drawing.Point(4, 179);
            this.lblPriceBuy.Name = "lblPriceBuy";
            this.lblPriceBuy.Size = new System.Drawing.Size(73, 13);
            this.lblPriceBuy.TabIndex = 5;
            this.lblPriceBuy.Text = "Ціна закупки";
            // 
            // lblPriceSell
            // 
            this.lblPriceSell.AutoSize = true;
            this.lblPriceSell.Location = new System.Drawing.Point(4, 205);
            this.lblPriceSell.Name = "lblPriceSell";
            this.lblPriceSell.Size = new System.Drawing.Size(75, 13);
            this.lblPriceSell.TabIndex = 6;
            this.lblPriceSell.Text = "Ціна продажу";
            // 
            // lblWarranty
            // 
            this.lblWarranty.AutoSize = true;
            this.lblWarranty.Location = new System.Drawing.Point(4, 231);
            this.lblWarranty.Name = "lblWarranty";
            this.lblWarranty.Size = new System.Drawing.Size(75, 13);
            this.lblWarranty.TabIndex = 7;
            this.lblWarranty.Text = "Гарантія (міс)";
            // 
            // tbIdInDatabase
            // 
            this.tbIdInDatabase.Enabled = false;
            this.tbIdInDatabase.Location = new System.Drawing.Point(153, 2);
            this.tbIdInDatabase.Name = "tbIdInDatabase";
            this.tbIdInDatabase.ReadOnly = true;
            this.tbIdInDatabase.Size = new System.Drawing.Size(139, 20);
            this.tbIdInDatabase.TabIndex = 8;
            // 
            // ddlCategory
            // 
            this.ddlCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCategory.FormattingEnabled = true;
            this.ddlCategory.Location = new System.Drawing.Point(66, 52);
            this.ddlCategory.Name = "ddlCategory";
            this.ddlCategory.Size = new System.Drawing.Size(226, 21);
            this.ddlCategory.TabIndex = 9;
            this.ddlCategory.SelectedIndexChanged += new System.EventHandler(this.ddlCategory_SelectedIndexChanged);
            // 
            // ddlSubcategory
            // 
            this.ddlSubcategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSubcategory.FormattingEnabled = true;
            this.ddlSubcategory.Location = new System.Drawing.Point(81, 101);
            this.ddlSubcategory.Name = "ddlSubcategory";
            this.ddlSubcategory.Size = new System.Drawing.Size(211, 21);
            this.ddlSubcategory.TabIndex = 10;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(49, 126);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(243, 20);
            this.tbName.TabIndex = 11;
            // 
            // tbCount
            // 
            this.tbCount.Location = new System.Drawing.Point(63, 150);
            this.tbCount.Name = "tbCount";
            this.tbCount.Size = new System.Drawing.Size(63, 20);
            this.tbCount.TabIndex = 12;
            this.tbCount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbCount_KeyPress);
            // 
            // tbPriceBuy
            // 
            this.tbPriceBuy.Location = new System.Drawing.Point(81, 176);
            this.tbPriceBuy.Name = "tbPriceBuy";
            this.tbPriceBuy.Size = new System.Drawing.Size(100, 20);
            this.tbPriceBuy.TabIndex = 13;
            this.tbPriceBuy.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPriceBuy_KeyPress);
            // 
            // tbPriceSell
            // 
            this.tbPriceSell.Location = new System.Drawing.Point(81, 202);
            this.tbPriceSell.Name = "tbPriceSell";
            this.tbPriceSell.Size = new System.Drawing.Size(100, 20);
            this.tbPriceSell.TabIndex = 14;
            this.tbPriceSell.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPriceSell_KeyPress);
            // 
            // tbWarranty
            // 
            this.tbWarranty.Location = new System.Drawing.Point(81, 228);
            this.tbWarranty.Name = "tbWarranty";
            this.tbWarranty.Size = new System.Drawing.Size(100, 20);
            this.tbWarranty.TabIndex = 15;
            this.tbWarranty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbWarranty_KeyPress);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(153, 254);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(139, 34);
            this.btnConfirm.TabIndex = 16;
            this.btnConfirm.Text = "Підтвердити";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btnConfirm_KeyUp);
            this.btnConfirm.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnConfirm_MouseUp);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(7, 255);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(139, 33);
            this.btnCancel.TabIndex = 17;
            this.btnCancel.Text = "Відмінити";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btnCancel_KeyUp);
            this.btnCancel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnCancel_MouseUp);
            // 
            // lblCurrentCategory
            // 
            this.lblCurrentCategory.AutoSize = true;
            this.lblCurrentCategory.ForeColor = System.Drawing.Color.Firebrick;
            this.lblCurrentCategory.Location = new System.Drawing.Point(78, 33);
            this.lblCurrentCategory.Name = "lblCurrentCategory";
            this.lblCurrentCategory.Size = new System.Drawing.Size(24, 13);
            this.lblCurrentCategory.TabIndex = 18;
            this.lblCurrentCategory.Text = "test";
            // 
            // lblCurrentSubcategory
            // 
            this.lblCurrentSubcategory.AutoSize = true;
            this.lblCurrentSubcategory.ForeColor = System.Drawing.Color.Firebrick;
            this.lblCurrentSubcategory.Location = new System.Drawing.Point(78, 85);
            this.lblCurrentSubcategory.Name = "lblCurrentSubcategory";
            this.lblCurrentSubcategory.Size = new System.Drawing.Size(24, 13);
            this.lblCurrentSubcategory.TabIndex = 19;
            this.lblCurrentSubcategory.Text = "test";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Firebrick;
            this.label1.Location = new System.Drawing.Point(22, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Поточна";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Firebrick;
            this.label2.Location = new System.Drawing.Point(23, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Поточна";
            // 
            // FormGoodEdit
            // 
            this.AcceptButton = this.btnConfirm;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(295, 287);
            this.ControlBox = false;
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblCurrentSubcategory);
            this.Controls.Add(this.lblCurrentCategory);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.tbWarranty);
            this.Controls.Add(this.tbPriceSell);
            this.Controls.Add(this.tbPriceBuy);
            this.Controls.Add(this.tbCount);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.ddlSubcategory);
            this.Controls.Add(this.ddlCategory);
            this.Controls.Add(this.tbIdInDatabase);
            this.Controls.Add(this.lblWarranty);
            this.Controls.Add(this.lblPriceSell);
            this.Controls.Add(this.lblPriceBuy);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblSubcategory);
            this.Controls.Add(this.lblCategory);
            this.Controls.Add(this.lblID);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormGoodEdit";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Редагування товару";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Label lblCategory;
        private System.Windows.Forms.Label lblSubcategory;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.Label lblPriceBuy;
        private System.Windows.Forms.Label lblPriceSell;
        private System.Windows.Forms.Label lblWarranty;
        private System.Windows.Forms.TextBox tbIdInDatabase;
        private System.Windows.Forms.ComboBox ddlCategory;
        private System.Windows.Forms.ComboBox ddlSubcategory;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox tbCount;
        private System.Windows.Forms.TextBox tbPriceBuy;
        private System.Windows.Forms.TextBox tbPriceSell;
        private System.Windows.Forms.TextBox tbWarranty;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblCurrentCategory;
        private System.Windows.Forms.Label lblCurrentSubcategory;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}