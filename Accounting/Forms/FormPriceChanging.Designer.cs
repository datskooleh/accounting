﻿namespace Accounting.Forms
{
    partial class FormPriceChanging
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableGoods = new System.Windows.Forms.DataGridView();
            this.tableName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableOld = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableNew = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.tableGoods)).BeginInit();
            this.SuspendLayout();
            // 
            // tableGoods
            // 
            this.tableGoods.AllowUserToAddRows = false;
            this.tableGoods.AllowUserToDeleteRows = false;
            this.tableGoods.AllowUserToResizeRows = false;
            this.tableGoods.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableGoods.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableGoods.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tableName,
            this.tableDate,
            this.tableOld,
            this.tableNew});
            this.tableGoods.Location = new System.Drawing.Point(2, 1);
            this.tableGoods.MultiSelect = false;
            this.tableGoods.Name = "tableGoods";
            this.tableGoods.ReadOnly = true;
            this.tableGoods.RowHeadersVisible = false;
            this.tableGoods.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tableGoods.Size = new System.Drawing.Size(459, 258);
            this.tableGoods.TabIndex = 0;
            // 
            // tableName
            // 
            this.tableName.HeaderText = "Назва";
            this.tableName.Name = "tableName";
            this.tableName.ReadOnly = true;
            this.tableName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.tableName.Width = 140;
            // 
            // tableDate
            // 
            this.tableDate.HeaderText = "Дата зміни";
            this.tableDate.Name = "tableDate";
            this.tableDate.ReadOnly = true;
            this.tableDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.tableDate.Width = 105;
            // 
            // tableOld
            // 
            this.tableOld.HeaderText = "Стара ціна";
            this.tableOld.Name = "tableOld";
            this.tableOld.ReadOnly = true;
            this.tableOld.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.tableOld.Width = 105;
            // 
            // tableNew
            // 
            this.tableNew.HeaderText = "Нова ціна";
            this.tableNew.Name = "tableNew";
            this.tableNew.ReadOnly = true;
            this.tableNew.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.tableNew.Width = 105;
            // 
            // FormPriceChanging
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(463, 261);
            this.Controls.Add(this.tableGoods);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormPriceChanging";
            this.Text = "Історія змін ціни продажу товару";
            ((System.ComponentModel.ISupportInitialize)(this.tableGoods)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView tableGoods;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableName;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableOld;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableNew;
    }
}