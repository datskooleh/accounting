﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Forms
{
    public partial class FormGoodSelect : Form
    {
        private List<Source.Types.Good> _allGoods;
        private LinkedList<Source.Types.Good> _goods;
        LinkedList<String> _serials = null;
        private Source.Data.Category category;

        public FormGoodSelect(List<Source.Types.Good> allGoods, LinkedList<Source.Types.Good> goods, LinkedList<String> serials)
        {
            InitializeComponent();

            _allGoods = allGoods;
            category = Source.Data.Category.Init();
            _serials = serials;

            _goods = goods;

            foreach (var item in category.Sub)
                ddlSubcategory.Items.Add(item.Name);

            foreach (var item in category.Main)
                ddlCategory.Items.Add(item.Name);

            if (ddlCategory.Items.Count > 0)
                ddlCategory.SelectedItem = ddlCategory.Items[0];
        }

        private void btnCancel_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DialogResult = System.Windows.Forms.DialogResult.Cancel;

                this.Close();
            }
        }

        private void btnCancel_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                btnCancel_KeyUp(sender, new KeyEventArgs(Keys.Enter));
        }

        private void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReparseDropDownList(ddlCategory, ddlSubcategory);
        }

        private void ddlSubcategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            tableGoods.Rows.Clear();

            ReparseTable();
        }



        private void ReparseDropDownList(ComboBox main, ComboBox sub)
        {
            ddlSubcategory.Items.Clear();

            Int32 id = category.Main
                .Where(x => x.Name
                    .Equals(main.SelectedItem.ToString()))
                .Single()
                .Id;

            foreach (Source.Types.Subcategory item in category.Sub)
                if (id == item.IdCategory)
                    sub.Items.Add(item.Name);

            if (ddlSubcategory.Items.Count == 0)
            {
                Message.Show.Error("В даній категорії не існує підкатегорій");
                return;
            }
            else
                ddlSubcategory.SelectedIndex = 0;
        }

        private void ReparseTable()
        {
            Int32 idCategory = category.Main[ddlCategory.SelectedIndex].Id;
            Int32 idSubcategory = category.Sub
                .Where(x => x.IdCategory == idCategory && ddlSubcategory.SelectedItem.ToString().Equals(x.Name))
                .Select(x => x.Id)
                .Single();

            foreach (Source.Types.Good good in _allGoods)
                if (good.IdCategory == idSubcategory)
                    AddToGoodsTable(good, idCategory);
        }

        private void AddToGoodsTable(Source.Types.Good item, Int32 idCategory)
        {
            int rowIndex;
            DataGridViewRow row;

            rowIndex = tableGoods.Rows.Add();
            row = tableGoods.Rows[rowIndex];
            row.Cells[0].Value = item.Name;
            row.Cells[1].Value = item.Count;
            row.Cells[2].Value = item.PriceSell;
            row.Cells[3].Value = item.Warranty;
            row.Cells[4].Value = item.Id;
            row.Cells[5].Value = item.IdCategory;
            row.Cells[6].Value = idCategory;
        }

        private void tbCount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || Char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        private void tbCount_TextChanged(object sender, EventArgs e)
        {
            if(tableGoods.SelectedRows.Count == 0)
            {
                Message.Show.Warning("Спершу потрібно вибрати товар");
                tbCount.Text = "";
            }
            else if (tbCount.Text.Length > 0)
            {
                if (Convert.ToInt32(tbCount.Text) > Convert.ToInt32(tableGoods.SelectedRows[0].Cells[1].Value.ToString()))
                {
                    Message.Show.Error("В наявності не має такої кількості товару");
                    tbCount.Text = tbCount.Text.Remove(tbCount.Text.Length - 1);
                }
                else
                    tbTotalAmount.Text = Convert.ToString(
                        Math.Round(Convert.ToDecimal(tbCount.Text) * Convert.ToDecimal(tableGoods.SelectedRows[0].Cells[2].Value), 2));
            }
            else
                tbTotalAmount.Text = "";
        }

        private void btnConfirm_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (tableGoods.SelectedRows.Count == 0 || tableGoods.SelectedCells.Count == 0)
                {
                    Message.Show.Information("Потрібно вибрати товар зі списку");
                    DialogResult = System.Windows.Forms.DialogResult.None;
                }
                else
                {
                    if (tbCount.Text.Length == 0)
                    {
                        Message.Show.Error("Не введено кількість товару для покупки");
                        DialogResult = System.Windows.Forms.DialogResult.None;
                    }
                    else if (Convert.ToInt32(tbCount.Text) < 1)
                    {
                        Message.Show.Error("Кількість товару не є вірною");
                        DialogResult = System.Windows.Forms.DialogResult.None;
                    }
                    else
                    {
                        Int32? warranty;

                        try
                        {
                            warranty = Convert.ToInt32(tableGoods.SelectedRows[0].Cells[3].Value.ToString());
                        }
                        catch(NullReferenceException ex)
                        {
                            warranty = null;
                        }

                        DialogResult hasSerials = System.Windows.Forms.DialogResult.No;

                        if (warranty.HasValue)
                            hasSerials = Message.Show.Question("Даний товар має гарантійний термін. Ввести серійні номери?", "Гарантія", MessageBoxButtons.YesNo);

                        FormGoodSelectedSN form;

                        if (hasSerials == System.Windows.Forms.DialogResult.Yes)
                        {
                            form = new FormGoodSelectedSN(_serials, Convert.ToInt32(tbCount.Text), FormGoodSelectedSN.OpenType.Add);
                            form.TopMost = true;
                            form.ShowDialog();
                        }

                        Source.Types.Good good = new Source.Types.Good(Convert.ToInt32(tableGoods.SelectedRows[0].Cells[4].Value.ToString()),
                            Convert.ToInt32(tableGoods.SelectedRows[0].Cells[5].Value.ToString()),
                            tableGoods.SelectedRows[0].Cells[0].Value.ToString(),
                            Convert.ToInt32(tbCount.Text),
                            0,
                            Convert.ToDecimal(tableGoods.SelectedRows[0].Cells[2].Value.ToString()),
                            warranty.HasValue ? warranty.Value : (Int32?)null);

                        _goods.AddLast(good);

                        Source.Types.Good g = _allGoods.Where(x => x.Id == good.Id).Single();
                        g.Count -= Convert.ToInt32(tbCount.Text);

                        DialogResult = System.Windows.Forms.DialogResult.OK;
                    }
                }
            }
        }

        private void btnConfirm_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                btnConfirm_KeyUp(sender, new KeyEventArgs(Keys.Enter));
        }

        private void tableGoods_SelectionChanged(object sender, EventArgs e)
        {
            tbCount.Text = "";
        }
    }
}
