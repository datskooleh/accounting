﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Accounting.Forms
{
    public partial class FormCategory : Form
    {
        private Source.Data.Category category;
        private Int32 previousSelectedCategoryIndex;
        private Int32 currentlySelectedCategoryIndex;

        public FormCategory()
        {
            InitializeComponent();

            category = Source.Data.Category.Init();

            lvCategory.Items.Add("1").SubItems.Add("Всі");

            foreach (Source.Types.Category item in category.Main)
                lvCategory.Items.Add(Convert.ToString(lvCategory.Items.Count + 1)).SubItems.Add(item.Name);

            foreach (Source.Types.Subcategory item in category.Sub)
                lvSubcategory.Items.Add(Convert.ToString(lvSubcategory.Items.Count + 1)).SubItems.Add(item.Name);

            previousSelectedCategoryIndex = -1;
            currentlySelectedCategoryIndex = 0;
        }

        #region Add items to main category

        private void btnAddCategory_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!HasText(tbCategory.Text))
                {
                    Message.Show.Warning("Не задано назви підкатегорії категорії", "Попередження");
                    return;
                }

                if (IsInList(lvCategory, tbCategory.Text))
                {
                    Message.Show.Information("Дана категорія вже присутня", "Увага");
                    return;
                }

                String number = Convert.ToString(lvCategory.Items.Count + 1);
                lvCategory.Items.Add(number).SubItems.Add(tbCategory.Text);

                category.Main.Add(Source.DB.Write.Category(tbCategory.Text));
            }
        }

        private void btnAddCategory_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                btnAddCategory_KeyUp(sender, new KeyEventArgs(Keys.Enter));
        }

        #endregion

        #region Add items to subcategory

        private void btnAddSubcategory_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (!HasText(tbSubcategory.Text))
                {
                    Message.Show.Warning("Не задано назви категорії", "Попередження");
                    return;
                }

                if (IsInList(lvSubcategory, tbSubcategory.Text))
                {
                    Message.Show.Information("Дана категорія вже присутня", "Увага");
                    return;
                }
                if (currentlySelectedCategoryIndex == 0)
                {
                    Message.Show.Information("Не можливо дадти в кореневу категорію \"Всі\". "
                        + "Виберіть іншу.", "Увага");
                    return;
                }

                String number = Convert.ToString(lvSubcategory.Items.Count + 1);
                lvSubcategory.Items.Add(number).SubItems.Add(tbSubcategory.Text);

                Source.Types.Subcategory cat =
                    Source.DB.Write
                    .Subcategory(category.Main
                    .ElementAt(currentlySelectedCategoryIndex - 1).Id, tbSubcategory.Text);
                
                category.Sub.Add(cat);
            }
        }

        private void btnAddSubcategory_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                btnAddSubcategory_KeyUp(sender, new KeyEventArgs(Keys.Enter));
        }

        #endregion

        #region TextBox input control

        private void tbCategory_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnAddCategory_KeyUp(sender, e);
        }

        private void tbSubcategory_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnAddSubcategory_KeyUp(sender, e);
        }

        #endregion

        private void lvCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvCategory.SelectedIndices.Count == 0)
                return;

            else if (lvCategory.SelectedIndices[0] == currentlySelectedCategoryIndex)
                return;
            
            previousSelectedCategoryIndex = currentlySelectedCategoryIndex;
            currentlySelectedCategoryIndex = lvCategory.SelectedIndices[0];

            ResetSubcategoryList();
        }

        private void lvCategory_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if (currentlySelectedCategoryIndex == 0)
                    return;

                Boolean IsAlone = true;

                foreach(Source.Types.Subcategory item in category.Sub)
                    if (item.IdCategory == category.Main[currentlySelectedCategoryIndex - 1].Id)
                    {
                        IsAlone = false;
                        break;
                    }

                if (!IsAlone)
                {
                    Message.Show.Error("Не можливо видалити категорію"
                        + " в яку входять підкатегорії. Видаліть спершу пов\'язані підкатегорії",
                        "Помилка");
                    return;
                }

                Source.DB.Delete.Category(category.Main[currentlySelectedCategoryIndex - 1].Id);

                category.Main.RemoveAt(currentlySelectedCategoryIndex - 1);
                lvCategory.Items.RemoveAt(currentlySelectedCategoryIndex);

                ResetNumeration(lvCategory);
            }
        }

        private void lvSubcategory_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                if(lvSubcategory.SelectedIndices.Count == 0)
                    return;

                var subID = category.Sub
                    .Where(x => x.IdCategory == category.Main[currentlySelectedCategoryIndex - 1].Id)
                    .Skip(lvSubcategory.SelectedIndices[0])
                    .Take(1)
                    .Select(x => x.Id)
                    .Single();

                foreach (Source.Types.Good item in Source.DB.Read.Goods())
                    if (item.IdCategory == subID)
                    {
                        Message.Show.Error("Не можливо видалити підкатегорію до якої відноситься товар", "Помилка");
                        return;
                    }

                Source.DB.Delete.Subcategory(category.Sub[lvSubcategory.SelectedIndices[0]].Id);

                lvSubcategory.Items.RemoveAt(lvSubcategory.SelectedIndices[0]);

                category.Sub.Remove(category.Sub.Select(x => x).Where(x => x.Id == subID).Single());

                ResetNumeration(lvSubcategory);
            }
        }






        private Boolean HasText(String input)
        {
            return !String.IsNullOrWhiteSpace(input);
        }

        private Boolean IsInList(ListView lv, String value)
        {
            for (int i = 0; i < lv.Items.Count; i++)
            {
                if (lv.Items[i].SubItems[1].Text.Equals(value))
                    return true;
            }

            return false;
        }

        private void ResetNumeration(ListView lv)
        {
            for (int i = 0; i < lv.Items.Count; i++)
                lv.Items[i].Text = Convert.ToString(i + 1);
        }

        private void ResetSubcategoryList()
        {
            lvSubcategory.Items.Clear();

            if (currentlySelectedCategoryIndex == 0)
                foreach (Source.Types.Subcategory item in category.Sub)
                    lvSubcategory.Items.Add(Convert.ToString(lvSubcategory.Items.Count + 1)).SubItems.Add(item.Name);
            else
                foreach (Source.Types.Subcategory item in category.Sub)
                    if (item.IdCategory == category.Main[currentlySelectedCategoryIndex - 1].Id)
                        lvSubcategory.Items.Add(Convert.ToString(lvSubcategory.Items.Count + 1)).SubItems.Add(item.Name);
        }

        private void lvCategory_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (lvCategory.SelectedIndices[0] == 0)
                return;

            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                TextBox tb = new TextBox();

                tb.Location = new Point(e.X, e.Y);
                tb.RectangleToClient(lvCategory.GetItemRect(lvCategory.SelectedIndices[0]));
                tb.Enabled = true;
                tb.Show();
                //tb.DrawToBitmap();
                /*
                if (tb.Text.Length > 0)
                    lvCategory_AfterLabelEdit(sender, new LabelEditEventArgs(lvCategory.SelectedIndices[0], tb.Text));
                /*
                tb.Clear();
                tb.Dispose();*/

                //lvCategory.Items[lvCategory.SelectedIndices[0]].BeginEdit();

                //Message.Show.Information("Need to be implemented", "Info");
            }
        }

        private void lvSubcategory_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                Message.Show.Information("Need to be implemented", "Info");
            }
        }

        private void lvCategory_AfterLabelEdit(object sender, LabelEditEventArgs e)
        {
            lvCategory.Items[e.Item].SubItems[1].Text = e.Label;

            Message.Show.Information("Need to be implemented", "Info");
        }

        private void lvSubcategory_AfterLabelEdit(object sender, LabelEditEventArgs e)
        {
            Message.Show.Information("Need to be implemented", "Info");
        }

        private void FormCategory_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'accounting_dbDataSet.Category' table. You can move, or remove it, as needed.
            this.categoryTableAdapter.Fill(this.accounting_dbDataSet.Category);

        }
    }
}
