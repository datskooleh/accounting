﻿namespace Accounting.Forms
{
    partial class FormCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lvCategory = new System.Windows.Forms.ListView();
            this.lvColumnNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvColumnName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblArrow = new System.Windows.Forms.Label();
            this.tbCategory = new System.Windows.Forms.TextBox();
            this.btnAddCategory = new System.Windows.Forms.Button();
            this.btnAddSubcategory = new System.Windows.Forms.Button();
            this.tbSubcategory = new System.Windows.Forms.TextBox();
            this.lvSubcategory = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.accounting_dbDataSet = new Accounting.accounting_dbDataSet();
            this.categoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.categoryTableAdapter = new Accounting.accounting_dbDataSetTableAdapters.CategoryTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.accounting_dbDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // lvCategory
            // 
            this.lvCategory.Alignment = System.Windows.Forms.ListViewAlignment.SnapToGrid;
            this.lvCategory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lvCategory.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvColumnNumber,
            this.lvColumnName});
            this.lvCategory.FullRowSelect = true;
            this.lvCategory.GridLines = true;
            this.lvCategory.HideSelection = false;
            this.lvCategory.LabelEdit = true;
            this.lvCategory.Location = new System.Drawing.Point(3, 3);
            this.lvCategory.MultiSelect = false;
            this.lvCategory.Name = "lvCategory";
            this.lvCategory.Size = new System.Drawing.Size(245, 272);
            this.lvCategory.TabIndex = 5;
            this.lvCategory.TabStop = false;
            this.lvCategory.UseCompatibleStateImageBehavior = false;
            this.lvCategory.View = System.Windows.Forms.View.Details;
            this.lvCategory.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.lvCategory_AfterLabelEdit);
            this.lvCategory.SelectedIndexChanged += new System.EventHandler(this.lvCategory_SelectedIndexChanged);
            this.lvCategory.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lvCategory_KeyUp);
            this.lvCategory.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvCategory_MouseDoubleClick);
            // 
            // lvColumnNumber
            // 
            this.lvColumnNumber.Text = "№";
            this.lvColumnNumber.Width = 40;
            // 
            // lvColumnName
            // 
            this.lvColumnName.Text = "Назва";
            this.lvColumnName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.lvColumnName.Width = 200;
            // 
            // lblArrow
            // 
            this.lblArrow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblArrow.AutoSize = true;
            this.lblArrow.Font = new System.Drawing.Font("Microsoft Sans Serif", 25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblArrow.Location = new System.Drawing.Point(254, 127);
            this.lblArrow.Name = "lblArrow";
            this.lblArrow.Size = new System.Drawing.Size(41, 39);
            this.lblArrow.TabIndex = 4;
            this.lblArrow.Text = "→";
            this.lblArrow.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbCategory
            // 
            this.tbCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCategory.Location = new System.Drawing.Point(3, 278);
            this.tbCategory.Name = "tbCategory";
            this.tbCategory.Size = new System.Drawing.Size(164, 23);
            this.tbCategory.TabIndex = 0;
            this.tbCategory.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbCategory_KeyUp);
            // 
            // btnAddCategory
            // 
            this.btnAddCategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddCategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCategory.Location = new System.Drawing.Point(173, 278);
            this.btnAddCategory.Name = "btnAddCategory";
            this.btnAddCategory.Size = new System.Drawing.Size(75, 23);
            this.btnAddCategory.TabIndex = 1;
            this.btnAddCategory.Text = "Додати";
            this.btnAddCategory.UseVisualStyleBackColor = true;
            this.btnAddCategory.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btnAddCategory_KeyUp);
            this.btnAddCategory.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnAddCategory_MouseUp);
            // 
            // btnAddSubcategory
            // 
            this.btnAddSubcategory.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnAddSubcategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddSubcategory.Location = new System.Drawing.Point(298, 278);
            this.btnAddSubcategory.Name = "btnAddSubcategory";
            this.btnAddSubcategory.Size = new System.Drawing.Size(75, 23);
            this.btnAddSubcategory.TabIndex = 2;
            this.btnAddSubcategory.Text = "Додати";
            this.btnAddSubcategory.UseVisualStyleBackColor = true;
            this.btnAddSubcategory.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btnAddSubcategory_KeyUp);
            this.btnAddSubcategory.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnAddSubcategory_MouseUp);
            // 
            // tbSubcategory
            // 
            this.tbSubcategory.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbSubcategory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSubcategory.Location = new System.Drawing.Point(379, 278);
            this.tbSubcategory.Name = "tbSubcategory";
            this.tbSubcategory.Size = new System.Drawing.Size(164, 23);
            this.tbSubcategory.TabIndex = 3;
            this.tbSubcategory.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tbSubcategory_KeyUp);
            // 
            // lvSubcategory
            // 
            this.lvSubcategory.Alignment = System.Windows.Forms.ListViewAlignment.SnapToGrid;
            this.lvSubcategory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lvSubcategory.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lvSubcategory.FullRowSelect = true;
            this.lvSubcategory.GridLines = true;
            this.lvSubcategory.HideSelection = false;
            this.lvSubcategory.LabelEdit = true;
            this.lvSubcategory.Location = new System.Drawing.Point(298, 3);
            this.lvSubcategory.MultiSelect = false;
            this.lvSubcategory.Name = "lvSubcategory";
            this.lvSubcategory.Size = new System.Drawing.Size(245, 272);
            this.lvSubcategory.TabIndex = 6;
            this.lvSubcategory.TabStop = false;
            this.lvSubcategory.UseCompatibleStateImageBehavior = false;
            this.lvSubcategory.View = System.Windows.Forms.View.Details;
            this.lvSubcategory.AfterLabelEdit += new System.Windows.Forms.LabelEditEventHandler(this.lvSubcategory_AfterLabelEdit);
            this.lvSubcategory.KeyUp += new System.Windows.Forms.KeyEventHandler(this.lvSubcategory_KeyUp);
            this.lvSubcategory.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lvSubcategory_MouseDoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "№";
            this.columnHeader1.Width = 40;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Назва";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 200;
            // 
            // accounting_dbDataSet
            // 
            this.accounting_dbDataSet.DataSetName = "accounting_dbDataSet";
            this.accounting_dbDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // categoryBindingSource
            // 
            this.categoryBindingSource.DataMember = "Category";
            this.categoryBindingSource.DataSource = this.accounting_dbDataSet;
            // 
            // categoryTableAdapter
            // 
            this.categoryTableAdapter.ClearBeforeFill = true;
            // 
            // FormCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 306);
            this.Controls.Add(this.lvSubcategory);
            this.Controls.Add(this.tbSubcategory);
            this.Controls.Add(this.btnAddSubcategory);
            this.Controls.Add(this.btnAddCategory);
            this.Controls.Add(this.tbCategory);
            this.Controls.Add(this.lblArrow);
            this.Controls.Add(this.lvCategory);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormCategory";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "FormCategory";
            this.Load += new System.EventHandler(this.FormCategory_Load);
            ((System.ComponentModel.ISupportInitialize)(this.accounting_dbDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoryBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lvCategory;
        private System.Windows.Forms.Label lblArrow;
        private System.Windows.Forms.TextBox tbCategory;
        private System.Windows.Forms.Button btnAddCategory;
        private System.Windows.Forms.Button btnAddSubcategory;
        private System.Windows.Forms.TextBox tbSubcategory;
        private System.Windows.Forms.ColumnHeader lvColumnNumber;
        private System.Windows.Forms.ColumnHeader lvColumnName;
        private System.Windows.Forms.ListView lvSubcategory;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private accounting_dbDataSet accounting_dbDataSet;
        private System.Windows.Forms.BindingSource categoryBindingSource;
        private accounting_dbDataSetTableAdapters.CategoryTableAdapter categoryTableAdapter;
    }
}