﻿namespace Accounting.Forms
{
    partial class FormGoodSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.ddlCategory = new System.Windows.Forms.ComboBox();
            this.ddlSubcategory = new System.Windows.Forms.ComboBox();
            this.lblCount = new System.Windows.Forms.Label();
            this.tableGoods = new System.Windows.Forms.DataGridView();
            this.tableName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tablePrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableWarranty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SubcategoryID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CategoryID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbCount = new System.Windows.Forms.TextBox();
            this.tbTotalAmount = new System.Windows.Forms.TextBox();
            this.btnConfirm = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.tableGoods)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancel.BackColor = System.Drawing.Color.DarkRed;
            this.btnCancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnCancel.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnCancel.Location = new System.Drawing.Point(1, 373);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(214, 33);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.TabStop = false;
            this.btnCancel.Text = "Відмінити";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btnCancel_KeyUp);
            this.btnCancel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnCancel_MouseUp);
            // 
            // ddlCategory
            // 
            this.ddlCategory.DropDownHeight = 65;
            this.ddlCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCategory.DropDownWidth = 300;
            this.ddlCategory.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddlCategory.FormattingEnabled = true;
            this.ddlCategory.IntegralHeight = false;
            this.ddlCategory.ItemHeight = 13;
            this.ddlCategory.Location = new System.Drawing.Point(1, 2);
            this.ddlCategory.MaxDropDownItems = 15;
            this.ddlCategory.Name = "ddlCategory";
            this.ddlCategory.Size = new System.Drawing.Size(232, 21);
            this.ddlCategory.TabIndex = 0;
            this.ddlCategory.SelectedIndexChanged += new System.EventHandler(this.ddlCategory_SelectedIndexChanged);
            // 
            // ddlSubcategory
            // 
            this.ddlSubcategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ddlSubcategory.BackColor = System.Drawing.SystemColors.Window;
            this.ddlSubcategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSubcategory.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ddlSubcategory.FormattingEnabled = true;
            this.ddlSubcategory.ItemHeight = 13;
            this.ddlSubcategory.Location = new System.Drawing.Point(266, 2);
            this.ddlSubcategory.MaxDropDownItems = 20;
            this.ddlSubcategory.MaxLength = 30;
            this.ddlSubcategory.Name = "ddlSubcategory";
            this.ddlSubcategory.Size = new System.Drawing.Size(263, 21);
            this.ddlSubcategory.TabIndex = 1;
            this.ddlSubcategory.SelectedIndexChanged += new System.EventHandler(this.ddlSubcategory_SelectedIndexChanged);
            // 
            // lblCount
            // 
            this.lblCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(417, 353);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(53, 13);
            this.lblCount.TabIndex = 5;
            this.lblCount.Text = "Кількість";
            // 
            // tableGoods
            // 
            this.tableGoods.AllowUserToAddRows = false;
            this.tableGoods.AllowUserToDeleteRows = false;
            this.tableGoods.AllowUserToResizeRows = false;
            this.tableGoods.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableGoods.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableGoods.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tableName,
            this.tableCount,
            this.tablePrice,
            this.tableWarranty,
            this.id,
            this.SubcategoryID,
            this.CategoryID});
            this.tableGoods.Location = new System.Drawing.Point(1, 30);
            this.tableGoods.MultiSelect = false;
            this.tableGoods.Name = "tableGoods";
            this.tableGoods.ReadOnly = true;
            this.tableGoods.RowHeadersVisible = false;
            this.tableGoods.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tableGoods.Size = new System.Drawing.Size(528, 317);
            this.tableGoods.TabIndex = 6;
            this.tableGoods.SelectionChanged += new System.EventHandler(this.tableGoods_SelectionChanged);
            // 
            // tableName
            // 
            this.tableName.HeaderText = "Назва";
            this.tableName.Name = "tableName";
            this.tableName.ReadOnly = true;
            this.tableName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.tableName.Width = 200;
            // 
            // tableCount
            // 
            this.tableCount.HeaderText = "Наявно (од.)";
            this.tableCount.Name = "tableCount";
            this.tableCount.ReadOnly = true;
            this.tableCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tablePrice
            // 
            this.tablePrice.HeaderText = "Ціна (од.)";
            this.tablePrice.Name = "tablePrice";
            this.tablePrice.ReadOnly = true;
            this.tablePrice.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // tableWarranty
            // 
            this.tableWarranty.HeaderText = "Гарантія (міс.)";
            this.tableWarranty.Name = "tableWarranty";
            this.tableWarranty.ReadOnly = true;
            this.tableWarranty.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.tableWarranty.Width = 120;
            // 
            // id
            // 
            this.id.HeaderText = "ID";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.id.Visible = false;
            // 
            // SubcategoryID
            // 
            this.SubcategoryID.HeaderText = "ID підкатегорії";
            this.SubcategoryID.Name = "SubcategoryID";
            this.SubcategoryID.ReadOnly = true;
            this.SubcategoryID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.SubcategoryID.Visible = false;
            // 
            // CategoryID
            // 
            this.CategoryID.HeaderText = "ID категорії";
            this.CategoryID.Name = "CategoryID";
            this.CategoryID.ReadOnly = true;
            this.CategoryID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CategoryID.Visible = false;
            // 
            // tbCount
            // 
            this.tbCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCount.Location = new System.Drawing.Point(476, 350);
            this.tbCount.Name = "tbCount";
            this.tbCount.Size = new System.Drawing.Size(53, 20);
            this.tbCount.TabIndex = 7;
            this.tbCount.Text = "0";
            this.tbCount.TextChanged += new System.EventHandler(this.tbCount_TextChanged);
            this.tbCount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbCount_KeyPress);
            // 
            // tbTotalAmount
            // 
            this.tbTotalAmount.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTotalAmount.BackColor = System.Drawing.Color.MidnightBlue;
            this.tbTotalAmount.Enabled = false;
            this.tbTotalAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.tbTotalAmount.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.tbTotalAmount.Location = new System.Drawing.Point(221, 378);
            this.tbTotalAmount.Name = "tbTotalAmount";
            this.tbTotalAmount.ReadOnly = true;
            this.tbTotalAmount.Size = new System.Drawing.Size(97, 23);
            this.tbTotalAmount.TabIndex = 8;
            this.tbTotalAmount.Text = "Загалом";
            this.tbTotalAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnConfirm
            // 
            this.btnConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConfirm.BackColor = System.Drawing.Color.DarkGreen;
            this.btnConfirm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConfirm.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.btnConfirm.Location = new System.Drawing.Point(320, 373);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(209, 33);
            this.btnConfirm.TabIndex = 10;
            this.btnConfirm.Text = "Підтвердити";
            this.btnConfirm.UseVisualStyleBackColor = false;
            this.btnConfirm.KeyUp += new System.Windows.Forms.KeyEventHandler(this.btnConfirm_KeyUp);
            this.btnConfirm.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnConfirm_MouseUp);
            // 
            // FormGoodSelect
            // 
            this.AcceptButton = this.btnConfirm;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(534, 411);
            this.ControlBox = false;
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.tbTotalAmount);
            this.Controls.Add(this.tbCount);
            this.Controls.Add(this.tableGoods);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.ddlSubcategory);
            this.Controls.Add(this.ddlCategory);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormGoodSelect";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Товари";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.tableGoods)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox ddlCategory;
        private System.Windows.Forms.ComboBox ddlSubcategory;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.DataGridView tableGoods;
        private System.Windows.Forms.TextBox tbCount;
        private System.Windows.Forms.TextBox tbTotalAmount;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableName;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn tablePrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableWarranty;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn SubcategoryID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CategoryID;
    }
}