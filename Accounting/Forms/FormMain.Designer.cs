﻿namespace Accounting
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TSM = new System.Windows.Forms.MenuStrip();
            this.TSMExit = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMExitInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMExitChange = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMExitExit = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMBuys = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMRealization = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMRealizationSells = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMRealizationRepair = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMRealizationAssembly = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMCategory = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMReport = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMReportRealization = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMReportGraph = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMReportDetailed = new System.Windows.Forms.ToolStripMenuItem();
            this.історіяЗміниЦінToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.серійніНомериToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TSM.SuspendLayout();
            this.SuspendLayout();
            // 
            // TSM
            // 
            this.TSM.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.TSM.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMExit,
            this.TSMBuys,
            this.TSMRealization,
            this.TSMCategory,
            this.TSMReport});
            this.TSM.Location = new System.Drawing.Point(0, 0);
            this.TSM.Name = "TSM";
            this.TSM.Size = new System.Drawing.Size(999, 24);
            this.TSM.TabIndex = 0;
            this.TSM.Text = "menuStrip1";
            // 
            // TSMExit
            // 
            this.TSMExit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMExitInfo,
            this.TSMExitChange,
            this.TSMExitExit});
            this.TSMExit.Name = "TSMExit";
            this.TSMExit.Size = new System.Drawing.Size(47, 20);
            this.TSMExit.Text = "Вихід";
            // 
            // TSMExitInfo
            // 
            this.TSMExitInfo.Name = "TSMExitInfo";
            this.TSMExitInfo.Size = new System.Drawing.Size(138, 22);
            this.TSMExitInfo.Text = "Інформація";
            this.TSMExitInfo.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TSMExitInfo_MouseUp);
            // 
            // TSMExitChange
            // 
            this.TSMExitChange.Name = "TSMExitChange";
            this.TSMExitChange.Size = new System.Drawing.Size(138, 22);
            this.TSMExitChange.Text = "Зміна";
            this.TSMExitChange.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TSMExitChange_MouseUp);
            // 
            // TSMExitExit
            // 
            this.TSMExitExit.Name = "TSMExitExit";
            this.TSMExitExit.Size = new System.Drawing.Size(138, 22);
            this.TSMExitExit.Text = "Вихід";
            this.TSMExitExit.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TSMExitExit_MouseUp);
            // 
            // TSMBuys
            // 
            this.TSMBuys.Name = "TSMBuys";
            this.TSMBuys.Size = new System.Drawing.Size(64, 20);
            this.TSMBuys.Text = "Закупки";
            this.TSMBuys.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TSMBuys_MouseUp);
            // 
            // TSMRealization
            // 
            this.TSMRealization.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMRealizationSells,
            this.TSMRealizationRepair,
            this.TSMRealizationAssembly});
            this.TSMRealization.Name = "TSMRealization";
            this.TSMRealization.Size = new System.Drawing.Size(75, 20);
            this.TSMRealization.Text = "Реалізація";
            // 
            // TSMRealizationSells
            // 
            this.TSMRealizationSells.Name = "TSMRealizationSells";
            this.TSMRealizationSells.Size = new System.Drawing.Size(119, 22);
            this.TSMRealizationSells.Text = "Продаж";
            this.TSMRealizationSells.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TSMRealizationSells_MouseUp);
            // 
            // TSMRealizationRepair
            // 
            this.TSMRealizationRepair.Name = "TSMRealizationRepair";
            this.TSMRealizationRepair.Size = new System.Drawing.Size(119, 22);
            this.TSMRealizationRepair.Text = "Ремонт";
            this.TSMRealizationRepair.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TSMRealizationRepair_MouseUp);
            // 
            // TSMRealizationAssembly
            // 
            this.TSMRealizationAssembly.Name = "TSMRealizationAssembly";
            this.TSMRealizationAssembly.Size = new System.Drawing.Size(119, 22);
            this.TSMRealizationAssembly.Text = "Монтаж";
            this.TSMRealizationAssembly.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TSMRealizationAssembly_MouseUp);
            // 
            // TSMCategory
            // 
            this.TSMCategory.Name = "TSMCategory";
            this.TSMCategory.Size = new System.Drawing.Size(68, 20);
            this.TSMCategory.Text = "Категорії";
            this.TSMCategory.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TSMCategory_MouseUp);
            // 
            // TSMReport
            // 
            this.TSMReport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMReportRealization,
            this.TSMReportGraph,
            this.TSMReportDetailed,
            this.історіяЗміниЦінToolStripMenuItem,
            this.серійніНомериToolStripMenuItem});
            this.TSMReport.Name = "TSMReport";
            this.TSMReport.Size = new System.Drawing.Size(67, 20);
            this.TSMReport.Text = "Звітність";
            // 
            // TSMReportRealization
            // 
            this.TSMReportRealization.Name = "TSMReportRealization";
            this.TSMReportRealization.Size = new System.Drawing.Size(178, 22);
            this.TSMReportRealization.Text = "Реалізація";
            this.TSMReportRealization.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TSMReportRealization_MouseUp);
            // 
            // TSMReportGraph
            // 
            this.TSMReportGraph.Name = "TSMReportGraph";
            this.TSMReportGraph.Size = new System.Drawing.Size(178, 22);
            this.TSMReportGraph.Text = "Фінансовий графік";
            // 
            // TSMReportDetailed
            // 
            this.TSMReportDetailed.Name = "TSMReportDetailed";
            this.TSMReportDetailed.Size = new System.Drawing.Size(178, 22);
            this.TSMReportDetailed.Text = "Детальна";
            // 
            // історіяЗміниЦінToolStripMenuItem
            // 
            this.історіяЗміниЦінToolStripMenuItem.Name = "історіяЗміниЦінToolStripMenuItem";
            this.історіяЗміниЦінToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.історіяЗміниЦінToolStripMenuItem.Text = "Історія зміни цін";
            this.історіяЗміниЦінToolStripMenuItem.MouseUp += new System.Windows.Forms.MouseEventHandler(this.історіяЗміниЦінToolStripMenuItem_MouseUp);
            // 
            // серійніНомериToolStripMenuItem
            // 
            this.серійніНомериToolStripMenuItem.Name = "серійніНомериToolStripMenuItem";
            this.серійніНомериToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.серійніНомериToolStripMenuItem.Text = "Серійні номери";
            this.серійніНомериToolStripMenuItem.MouseUp += new System.Windows.Forms.MouseEventHandler(this.серійніНомериToolStripMenuItem_MouseUp);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 615);
            this.Controls.Add(this.TSM);
            this.MainMenuStrip = this.TSM;
            this.Name = "FormMain";
            this.Text = "Облік";
            this.TSM.ResumeLayout(false);
            this.TSM.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip TSM;
        private System.Windows.Forms.ToolStripMenuItem TSMExit;
        private System.Windows.Forms.ToolStripMenuItem TSMExitInfo;
        private System.Windows.Forms.ToolStripMenuItem TSMExitChange;
        private System.Windows.Forms.ToolStripMenuItem TSMExitExit;
        private System.Windows.Forms.ToolStripMenuItem TSMBuys;
        private System.Windows.Forms.ToolStripMenuItem TSMRealization;
        private System.Windows.Forms.ToolStripMenuItem TSMRealizationSells;
        private System.Windows.Forms.ToolStripMenuItem TSMRealizationRepair;
        private System.Windows.Forms.ToolStripMenuItem TSMRealizationAssembly;
        private System.Windows.Forms.ToolStripMenuItem TSMReport;
        private System.Windows.Forms.ToolStripMenuItem TSMReportDetailed;
        private System.Windows.Forms.ToolStripMenuItem TSMReportGraph;
        private System.Windows.Forms.ToolStripMenuItem TSMCategory;
        private System.Windows.Forms.ToolStripMenuItem TSMReportRealization;
        private System.Windows.Forms.ToolStripMenuItem історіяЗміниЦінToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem серійніНомериToolStripMenuItem;

    }
}

