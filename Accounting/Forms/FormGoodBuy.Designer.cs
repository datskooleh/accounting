﻿namespace Accounting.Forms
{
    partial class FormGoodBuy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbAdd = new System.Windows.Forms.GroupBox();
            this.lblAddSubdirectory = new System.Windows.Forms.Label();
            this.ddlAddSubcategory = new System.Windows.Forms.ComboBox();
            this.tbAddWarranty = new System.Windows.Forms.TextBox();
            this.tbAddSellPrice = new System.Windows.Forms.TextBox();
            this.tbAddBuyPrice = new System.Windows.Forms.TextBox();
            this.tbAddCount = new System.Windows.Forms.TextBox();
            this.tbAddName = new System.Windows.Forms.TextBox();
            this.ddlAddCategory = new System.Windows.Forms.ComboBox();
            this.lblAddWarranty = new System.Windows.Forms.Label();
            this.lblAddPriceSell = new System.Windows.Forms.Label();
            this.lblAddPriceBuy = new System.Windows.Forms.Label();
            this.lblAddCount = new System.Windows.Forms.Label();
            this.lblAddName = new System.Windows.Forms.Label();
            this.lblAddCategory = new System.Windows.Forms.Label();
            this.tableGoods = new System.Windows.Forms.DataGridView();
            this.btnAddConfirm = new System.Windows.Forms.Button();
            this.ddlCategory = new System.Windows.Forms.ComboBox();
            this.ddlSubcategory = new System.Windows.Forms.ComboBox();
            this.gvTableNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gvTableSubcategoryId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gvTableName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gvTableCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gvTablePriceBuy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gvTablePriceSell = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gvTableWarranty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gvTableId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbAdd.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableGoods)).BeginInit();
            this.SuspendLayout();
            // 
            // gbAdd
            // 
            this.gbAdd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbAdd.Controls.Add(this.lblAddSubdirectory);
            this.gbAdd.Controls.Add(this.ddlAddSubcategory);
            this.gbAdd.Controls.Add(this.tbAddWarranty);
            this.gbAdd.Controls.Add(this.tbAddSellPrice);
            this.gbAdd.Controls.Add(this.tbAddBuyPrice);
            this.gbAdd.Controls.Add(this.tbAddCount);
            this.gbAdd.Controls.Add(this.tbAddName);
            this.gbAdd.Controls.Add(this.ddlAddCategory);
            this.gbAdd.Controls.Add(this.lblAddWarranty);
            this.gbAdd.Controls.Add(this.lblAddPriceSell);
            this.gbAdd.Controls.Add(this.lblAddPriceBuy);
            this.gbAdd.Controls.Add(this.lblAddCount);
            this.gbAdd.Controls.Add(this.lblAddName);
            this.gbAdd.Controls.Add(this.lblAddCategory);
            this.gbAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbAdd.Location = new System.Drawing.Point(562, 3);
            this.gbAdd.Name = "gbAdd";
            this.gbAdd.Size = new System.Drawing.Size(208, 340);
            this.gbAdd.TabIndex = 0;
            this.gbAdd.TabStop = false;
            this.gbAdd.Text = "Додати";
            // 
            // lblAddSubdirectory
            // 
            this.lblAddSubdirectory.AutoSize = true;
            this.lblAddSubdirectory.Location = new System.Drawing.Point(3, 70);
            this.lblAddSubdirectory.Name = "lblAddSubdirectory";
            this.lblAddSubdirectory.Size = new System.Drawing.Size(91, 17);
            this.lblAddSubdirectory.TabIndex = 7;
            this.lblAddSubdirectory.Text = "Підкатегорія";
            // 
            // ddlAddSubcategory
            // 
            this.ddlAddSubcategory.DropDownHeight = 120;
            this.ddlAddSubcategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlAddSubcategory.DropDownWidth = 190;
            this.ddlAddSubcategory.FormattingEnabled = true;
            this.ddlAddSubcategory.IntegralHeight = false;
            this.ddlAddSubcategory.Location = new System.Drawing.Point(6, 90);
            this.ddlAddSubcategory.MaxDropDownItems = 15;
            this.ddlAddSubcategory.Name = "ddlAddSubcategory";
            this.ddlAddSubcategory.Size = new System.Drawing.Size(194, 24);
            this.ddlAddSubcategory.TabIndex = 6;
            // 
            // tbAddWarranty
            // 
            this.tbAddWarranty.Location = new System.Drawing.Point(135, 310);
            this.tbAddWarranty.MaxLength = 3;
            this.tbAddWarranty.Name = "tbAddWarranty";
            this.tbAddWarranty.Size = new System.Drawing.Size(67, 23);
            this.tbAddWarranty.TabIndex = 5;
            this.tbAddWarranty.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAddWarranty_KeyPress);
            // 
            // tbAddSellPrice
            // 
            this.tbAddSellPrice.Location = new System.Drawing.Point(103, 271);
            this.tbAddSellPrice.Name = "tbAddSellPrice";
            this.tbAddSellPrice.Size = new System.Drawing.Size(99, 23);
            this.tbAddSellPrice.TabIndex = 4;
            this.tbAddSellPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAddSellPrice_KeyPress);
            // 
            // tbAddBuyPrice
            // 
            this.tbAddBuyPrice.Location = new System.Drawing.Point(103, 230);
            this.tbAddBuyPrice.Name = "tbAddBuyPrice";
            this.tbAddBuyPrice.Size = new System.Drawing.Size(99, 23);
            this.tbAddBuyPrice.TabIndex = 3;
            this.tbAddBuyPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAddBuyPrice_KeyPress);
            // 
            // tbAddCount
            // 
            this.tbAddCount.Location = new System.Drawing.Point(78, 189);
            this.tbAddCount.MaxLength = 10;
            this.tbAddCount.Name = "tbAddCount";
            this.tbAddCount.Size = new System.Drawing.Size(124, 23);
            this.tbAddCount.TabIndex = 2;
            this.tbAddCount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAddCount_KeyPress);
            // 
            // tbAddName
            // 
            this.tbAddName.Location = new System.Drawing.Point(6, 146);
            this.tbAddName.Name = "tbAddName";
            this.tbAddName.Size = new System.Drawing.Size(196, 23);
            this.tbAddName.TabIndex = 1;
            // 
            // ddlAddCategory
            // 
            this.ddlAddCategory.DropDownHeight = 120;
            this.ddlAddCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlAddCategory.DropDownWidth = 190;
            this.ddlAddCategory.FormattingEnabled = true;
            this.ddlAddCategory.IntegralHeight = false;
            this.ddlAddCategory.Location = new System.Drawing.Point(6, 43);
            this.ddlAddCategory.MaxDropDownItems = 15;
            this.ddlAddCategory.Name = "ddlAddCategory";
            this.ddlAddCategory.Size = new System.Drawing.Size(196, 24);
            this.ddlAddCategory.TabIndex = 0;
            this.ddlAddCategory.SelectedIndexChanged += new System.EventHandler(this.ddlAddCategory_SelectedIndexChanged);
            // 
            // lblAddWarranty
            // 
            this.lblAddWarranty.AutoSize = true;
            this.lblAddWarranty.Location = new System.Drawing.Point(3, 313);
            this.lblAddWarranty.Name = "lblAddWarranty";
            this.lblAddWarranty.Size = new System.Drawing.Size(125, 17);
            this.lblAddWarranty.TabIndex = 5;
            this.lblAddWarranty.Text = "Гарантія (місяців)";
            // 
            // lblAddPriceSell
            // 
            this.lblAddPriceSell.AutoSize = true;
            this.lblAddPriceSell.Location = new System.Drawing.Point(3, 274);
            this.lblAddPriceSell.Name = "lblAddPriceSell";
            this.lblAddPriceSell.Size = new System.Drawing.Size(98, 17);
            this.lblAddPriceSell.TabIndex = 4;
            this.lblAddPriceSell.Text = "Ціна продажу";
            // 
            // lblAddPriceBuy
            // 
            this.lblAddPriceBuy.AutoSize = true;
            this.lblAddPriceBuy.Location = new System.Drawing.Point(3, 233);
            this.lblAddPriceBuy.Name = "lblAddPriceBuy";
            this.lblAddPriceBuy.Size = new System.Drawing.Size(94, 17);
            this.lblAddPriceBuy.TabIndex = 3;
            this.lblAddPriceBuy.Text = "Ціна закупки";
            // 
            // lblAddCount
            // 
            this.lblAddCount.AutoSize = true;
            this.lblAddCount.Location = new System.Drawing.Point(3, 192);
            this.lblAddCount.Name = "lblAddCount";
            this.lblAddCount.Size = new System.Drawing.Size(66, 17);
            this.lblAddCount.TabIndex = 2;
            this.lblAddCount.Text = "Кількість";
            // 
            // lblAddName
            // 
            this.lblAddName.AutoSize = true;
            this.lblAddName.Location = new System.Drawing.Point(3, 125);
            this.lblAddName.Name = "lblAddName";
            this.lblAddName.Size = new System.Drawing.Size(105, 17);
            this.lblAddName.TabIndex = 1;
            this.lblAddName.Text = "Найменування";
            // 
            // lblAddCategory
            // 
            this.lblAddCategory.AutoSize = true;
            this.lblAddCategory.Location = new System.Drawing.Point(3, 23);
            this.lblAddCategory.Name = "lblAddCategory";
            this.lblAddCategory.Size = new System.Drawing.Size(72, 17);
            this.lblAddCategory.TabIndex = 0;
            this.lblAddCategory.Text = "Категорія";
            // 
            // tableGoods
            // 
            this.tableGoods.AllowUserToAddRows = false;
            this.tableGoods.AllowUserToResizeRows = false;
            this.tableGoods.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableGoods.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableGoods.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.gvTableNumber,
            this.gvTableSubcategoryId,
            this.gvTableName,
            this.gvTableCount,
            this.gvTablePriceBuy,
            this.gvTablePriceSell,
            this.gvTableWarranty,
            this.gvTableId});
            this.tableGoods.Location = new System.Drawing.Point(5, 26);
            this.tableGoods.MultiSelect = false;
            this.tableGoods.Name = "tableGoods";
            this.tableGoods.ReadOnly = true;
            this.tableGoods.RowHeadersVisible = false;
            this.tableGoods.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tableGoods.Size = new System.Drawing.Size(551, 352);
            this.tableGoods.TabIndex = 1;
            this.tableGoods.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.tableGoods_CellMouseDoubleClick);
            this.tableGoods.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.tableGoods_UserDeletingRow);
            // 
            // btnAddConfirm
            // 
            this.btnAddConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddConfirm.Location = new System.Drawing.Point(562, 346);
            this.btnAddConfirm.Name = "btnAddConfirm";
            this.btnAddConfirm.Size = new System.Drawing.Size(208, 32);
            this.btnAddConfirm.TabIndex = 6;
            this.btnAddConfirm.Text = "Додати";
            this.btnAddConfirm.UseVisualStyleBackColor = true;
            this.btnAddConfirm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.btnAddConfirm_KeyPress);
            this.btnAddConfirm.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnAddConfirm_MouseUp);
            // 
            // ddlCategory
            // 
            this.ddlCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlCategory.FormattingEnabled = true;
            this.ddlCategory.Location = new System.Drawing.Point(5, 3);
            this.ddlCategory.MaxDropDownItems = 5;
            this.ddlCategory.Name = "ddlCategory";
            this.ddlCategory.Size = new System.Drawing.Size(269, 21);
            this.ddlCategory.TabIndex = 7;
            this.ddlCategory.SelectedIndexChanged += new System.EventHandler(this.ddlCategory_SelectedIndexChanged);
            // 
            // ddlSubcategory
            // 
            this.ddlSubcategory.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ddlSubcategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSubcategory.FormattingEnabled = true;
            this.ddlSubcategory.Location = new System.Drawing.Point(287, 3);
            this.ddlSubcategory.MaxDropDownItems = 5;
            this.ddlSubcategory.Name = "ddlSubcategory";
            this.ddlSubcategory.Size = new System.Drawing.Size(269, 21);
            this.ddlSubcategory.TabIndex = 8;
            this.ddlSubcategory.SelectedIndexChanged += new System.EventHandler(this.ddlSubcategory_SelectedIndexChanged);
            // 
            // gvTableNumber
            // 
            this.gvTableNumber.HeaderText = "№";
            this.gvTableNumber.Name = "gvTableNumber";
            this.gvTableNumber.ReadOnly = true;
            this.gvTableNumber.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.gvTableNumber.Width = 40;
            // 
            // gvTableSubcategoryId
            // 
            this.gvTableSubcategoryId.HeaderText = "Підкатегорія";
            this.gvTableSubcategoryId.Name = "gvTableSubcategoryId";
            this.gvTableSubcategoryId.ReadOnly = true;
            this.gvTableSubcategoryId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.gvTableSubcategoryId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // gvTableName
            // 
            this.gvTableName.HeaderText = "Назва";
            this.gvTableName.Name = "gvTableName";
            this.gvTableName.ReadOnly = true;
            this.gvTableName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.gvTableName.Width = 200;
            // 
            // gvTableCount
            // 
            this.gvTableCount.HeaderText = "Наявна кількість";
            this.gvTableCount.Name = "gvTableCount";
            this.gvTableCount.ReadOnly = true;
            this.gvTableCount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.gvTableCount.Width = 60;
            // 
            // gvTablePriceBuy
            // 
            this.gvTablePriceBuy.HeaderText = "Ціна покупки";
            this.gvTablePriceBuy.Name = "gvTablePriceBuy";
            this.gvTablePriceBuy.ReadOnly = true;
            this.gvTablePriceBuy.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.gvTablePriceBuy.Width = 80;
            // 
            // gvTablePriceSell
            // 
            this.gvTablePriceSell.HeaderText = "Ціна продажу";
            this.gvTablePriceSell.Name = "gvTablePriceSell";
            this.gvTablePriceSell.ReadOnly = true;
            this.gvTablePriceSell.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.gvTablePriceSell.Width = 80;
            // 
            // gvTableWarranty
            // 
            this.gvTableWarranty.HeaderText = "Гарантія (міс.)";
            this.gvTableWarranty.Name = "gvTableWarranty";
            this.gvTableWarranty.ReadOnly = true;
            this.gvTableWarranty.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.gvTableWarranty.Width = 60;
            // 
            // gvTableId
            // 
            this.gvTableId.HeaderText = "ID";
            this.gvTableId.Name = "gvTableId";
            this.gvTableId.ReadOnly = true;
            this.gvTableId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.gvTableId.Width = 50;
            // 
            // FormGoodBuy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 381);
            this.Controls.Add(this.ddlSubcategory);
            this.Controls.Add(this.ddlCategory);
            this.Controls.Add(this.btnAddConfirm);
            this.Controls.Add(this.tableGoods);
            this.Controls.Add(this.gbAdd);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormGoodBuy";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Закупки";
            this.gbAdd.ResumeLayout(false);
            this.gbAdd.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tableGoods)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbAdd;
        private System.Windows.Forms.DataGridView tableGoods;
        private System.Windows.Forms.Button btnAddConfirm;
        private System.Windows.Forms.Label lblAddWarranty;
        private System.Windows.Forms.Label lblAddPriceSell;
        private System.Windows.Forms.Label lblAddPriceBuy;
        private System.Windows.Forms.Label lblAddCount;
        private System.Windows.Forms.Label lblAddName;
        private System.Windows.Forms.Label lblAddCategory;
        private System.Windows.Forms.TextBox tbAddWarranty;
        private System.Windows.Forms.TextBox tbAddSellPrice;
        private System.Windows.Forms.TextBox tbAddBuyPrice;
        private System.Windows.Forms.TextBox tbAddCount;
        private System.Windows.Forms.TextBox tbAddName;
        private System.Windows.Forms.ComboBox ddlAddCategory;
        private System.Windows.Forms.Label lblAddSubdirectory;
        private System.Windows.Forms.ComboBox ddlAddSubcategory;
        private System.Windows.Forms.ComboBox ddlCategory;
        private System.Windows.Forms.ComboBox ddlSubcategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn gvTableNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn gvTableSubcategoryId;
        private System.Windows.Forms.DataGridViewTextBoxColumn gvTableName;
        private System.Windows.Forms.DataGridViewTextBoxColumn gvTableCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn gvTablePriceBuy;
        private System.Windows.Forms.DataGridViewTextBoxColumn gvTablePriceSell;
        private System.Windows.Forms.DataGridViewTextBoxColumn gvTableWarranty;
        private System.Windows.Forms.DataGridViewTextBoxColumn gvTableId;

    }
}