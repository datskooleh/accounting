﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Accounting.Forms
{
    public partial class FormGoodSelectedSN : Form
    {
        public enum OpenType
        {
            Edit,
            Add
        }

        private Int32 _countToAdd;

        private LinkedList<String> serials;

        public FormGoodSelectedSN(LinkedList<String> input, Int32 countToAdd, OpenType type)
        {
            InitializeComponent();

            _countToAdd = countToAdd;
            lvAdd.Items.Clear();

            serials = input;

            if (type == OpenType.Edit)
            {
                FillList();
            }
        }

        private void FillList()
        {
            foreach (String sn in serials)
                lvAdd.Items.Add(sn);

            serials.Clear();
        }

        private void btnAdd_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
                btnAdd_KeyUp(sender, new KeyEventArgs(Keys.Enter));
        }

        private void btnAdd_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (tbAdd.Text.Length == 0)
                {
                    Message.Show.Warning("Не введено жодного значення");
                }
                else if (_countToAdd == lvAdd.Items.Count)
                    Message.Show.Information("Не можливо додати більше серійних номерів ніж вибраних товарів");
                else
                {
                    for (int i = 0; i < lvAdd.Items.Count; i++)
                        if (tbAdd.Text.Equals(lvAdd.Items[i].Text))
                        {
                            Message.Show.Error("Цей номер вже присутній в списку");
                            return;
                        }

                    lvAdd.Items.Add(tbAdd.Text);
                    tbAdd.Clear();
                }
            }
        }

        private void FormGoodSelectedSN_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (lvAdd.Items.Count == 0)
            {
                DialogResult forceClose = Message.Show.Question("Не введено жодного номера", "Вийти?", MessageBoxButtons.YesNo);

                if (forceClose == System.Windows.Forms.DialogResult.No)
                    e.Cancel = true;
            }
            else if (lvAdd.Items.Count < _countToAdd)
            {
                DialogResult forceClose = Message.Show.Question("Кількість номерів менша ніж товарів", "Вийти?", MessageBoxButtons.YesNo);

                if (forceClose == System.Windows.Forms.DialogResult.No)
                    e.Cancel = true;
            }
            else
                for (int i = 0; i < lvAdd.Items.Count; i++)
                    serials.AddLast(lvAdd.Items[i].Text);
        }

        private void lvAdd_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                DialogResult toDelete = Message.Show.Question("Дійсно видалити серійний номер?", "Видалення серійного номера", MessageBoxButtons.YesNo);

                if (toDelete == System.Windows.Forms.DialogResult.Yes)
                {
                    lvAdd.SelectedItems[0].Remove();
                }
            }
        }
    }
}
