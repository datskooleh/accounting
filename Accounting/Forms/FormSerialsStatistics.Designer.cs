﻿namespace Accounting.Forms
{
    partial class FormSerialsStatistics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableSerials = new System.Windows.Forms.DataGridView();
            this.tableName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableBought = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableWarranty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableSN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.tableSerials)).BeginInit();
            this.SuspendLayout();
            // 
            // tableSerials
            // 
            this.tableSerials.AllowUserToAddRows = false;
            this.tableSerials.AllowUserToDeleteRows = false;
            this.tableSerials.AllowUserToResizeRows = false;
            this.tableSerials.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableSerials.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableSerials.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tableName,
            this.tableBought,
            this.tableWarranty,
            this.tableSN});
            this.tableSerials.Location = new System.Drawing.Point(2, 1);
            this.tableSerials.MultiSelect = false;
            this.tableSerials.Name = "tableSerials";
            this.tableSerials.ReadOnly = true;
            this.tableSerials.RowHeadersVisible = false;
            this.tableSerials.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tableSerials.Size = new System.Drawing.Size(606, 449);
            this.tableSerials.TabIndex = 0;
            // 
            // tableName
            // 
            this.tableName.HeaderText = "Назва";
            this.tableName.Name = "tableName";
            this.tableName.ReadOnly = true;
            this.tableName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.tableName.Width = 150;
            // 
            // tableBought
            // 
            this.tableBought.HeaderText = "Куплений";
            this.tableBought.Name = "tableBought";
            this.tableBought.ReadOnly = true;
            this.tableBought.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.tableBought.Width = 130;
            // 
            // tableWarranty
            // 
            this.tableWarranty.HeaderText = "Гарантія до";
            this.tableWarranty.Name = "tableWarranty";
            this.tableWarranty.ReadOnly = true;
            this.tableWarranty.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.tableWarranty.Width = 130;
            // 
            // tableSN
            // 
            this.tableSN.HeaderText = "Серійний номер";
            this.tableSN.Name = "tableSN";
            this.tableSN.ReadOnly = true;
            this.tableSN.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.tableSN.Width = 190;
            // 
            // FormSerialsStatistics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(612, 451);
            this.ControlBox = false;
            this.Controls.Add(this.tableSerials);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSerialsStatistics";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Статистика гарантії";
            ((System.ComponentModel.ISupportInitialize)(this.tableSerials)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView tableSerials;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableName;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableBought;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableWarranty;
        private System.Windows.Forms.DataGridViewTextBoxColumn tableSN;
    }
}