﻿using System;

using System.Windows.Forms;

namespace Accounting.Message
{
    public static class Show
    {
        public static void Warning(String text, String title = "Попередження")
        {
            MessageBox.Show(text, title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public static void Information(String text, String title = "Увага")
        {
            MessageBox.Show(text, title, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void Error(String text, String title = "Помилка")
        {
            MessageBox.Show(text, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public static DialogResult Question(String text, String title = "Підтвердження",
                                        MessageBoxButtons buttons = MessageBoxButtons.OKCancel)
        {
            DialogResult res = MessageBox.Show(text, title, buttons, MessageBoxIcon.Question);

            return res;
        }
    }
}
