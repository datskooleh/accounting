﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Data.SqlTypes;

namespace Accounting
{
    public static class Program
    {
        public static Source.DBConnection.Connection database;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            database = new Source.DBConnection.Connection();
            database.ConnectToDB();

            if (database.IsConnected)
                Application.Run(new FormMain());

            Application.Exit();
        }
    }
}
